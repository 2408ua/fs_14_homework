package hw12.pet;

public enum Species {
  UNKNOWN(false, 0, false),
  DOMESTIC_CAT(false, 4, true),
  ROBO_CAT(false, 4, false),
  DOG(false, 4, true),
  PIG(false, 4, true),
  PARROT(true, 2, false),
  HAMSTER(false, 4, true),
  FISH(false, 0, false);
  private final boolean canFly;
  private final int numberOfLegs;
  private final boolean hasFur;

  Species(boolean canFly, int numberOfLegs, boolean hasFur) {
    this.canFly = canFly;
    this.numberOfLegs = numberOfLegs;
    this.hasFur = hasFur;
  }

  public boolean isCanFly() {
    return canFly;
  }

  public int getNumberOfLegs() {
    return numberOfLegs;
  }

  public boolean isHasFur() {
    return hasFur;
  }

  @Override
  public String toString() {
    return "Species{" +
        "canFly=" + canFly +
        ", numberOfLegs=" + numberOfLegs +
        ", hasFur=" + hasFur +
        '}';
  }
}
