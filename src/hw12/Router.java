package hw12;

import hw12.family.FamilyController;

import java.util.HashMap;

public class Router {
  HashMap<Integer, Runnable> mainRouter;
  HashMap<Integer, Runnable> editRouter;
  FamilyController controller;

  public Router() {
    this.controller = new FamilyController();
    this.mainRouter = new HashMap<>();
    this.mainRouter.put(1, () -> this.controller.createTestsFamilies());
    this.mainRouter.put(2, () -> this.controller.displayAllFamilies());
    this.mainRouter.put(3, () -> this.controller.getFamiliesBiggerThan());
    this.mainRouter.put(4, () -> this.controller.getFamiliesLessThan());
    this.mainRouter.put(5, () -> this.controller.countFamiliesWithMemberNumber());
    this.mainRouter.put(6, () -> this.controller.createNewFamily());
    this.mainRouter.put(7, () -> this.controller.deleteFamilyByIndex());
    this.mainRouter.put(9, () -> this.controller.deleteAllChildrenOlderThen());
    this.mainRouter.put(10, () -> this.controller.writeFamiliesListToFile());
    this.mainRouter.put(11, () -> this.controller.loadFamiliesListFromFile());

    this.editRouter = new HashMap<>();
    this.editRouter.put(1, () -> this.controller.bornChild());
    this.editRouter.put(2, () -> this.controller.adoptChild());
  }

  public HashMap<Integer, Runnable> getMainRouter() {
    return this.mainRouter;
  }
  public HashMap<Integer, Runnable> getEditRouter() {
    return this.editRouter;
  }
}
