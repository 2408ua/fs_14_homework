package hw12.family;

import hw12.human.*;
import hw12.pet.Pet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;

public class Family implements HumanCreator, Serializable {
  private final HashSet<Pet> pets;
  private final Woman mother;
  private final Man father;
  private final ArrayList<Human> children;

  public Family(Woman mother, Man father) {
    this.mother = mother;
    this.father = father;
    this.mother.setFamily(this);
    this.father.setFamily(this);
    this.children = new ArrayList<Human>();
    this.pets = new HashSet<Pet>();
  }

  public Family(Human mother, Human father) {
    this.mother = (Woman) mother;
    this.father = (Man) father;
    this.mother.setFamily(this);
    this.father.setFamily(this);
    this.children = new ArrayList<Human>();
    this.pets = new HashSet<Pet>();
  }

  public HashSet<Pet> getPets() {
    return this.pets;
  }

  public Woman getMother() {
    return this.mother;
  }

  public Man getFather() {
    return this.father;
  }

  public ArrayList<Human> getChildren() {
    return this.children;
  }

  public void addChild(Human child) {
    child.setFamily(this);
    this.children.add(child);
  }

  public void addPet(Pet pet) {
    this.pets.add((Pet) pet);
    pet.setFamily(this);
  }

  public void deletePet(Pet pet) {
    if (this.pets.remove(pet)) {
      pet.setFamily(null);
    }
  }

  public boolean deleteChild(int index) {
    int length = this.children.size();
    if (length == 0 || length - 1 < index) return false;
    this.children.get(index).setFamily(null);
    this.children.remove(index);
    return true;
  }

  public int countFamily() {
    return this.children.size() + 2;
  }

  public int getCountChildren() {
    return this.children.size();
  }

  private boolean isNotChild(Human child) {
    for (Human item : this.children) {
      if (item.equals(child)) return false;
    }
    return true;
  }

  public boolean deleteChild(Human child) {
    if (children.size() == 0) return false;
    if (this.isNotChild(child)) return false;

    child.setFamily(null);
    children.remove(child);
    return true;
  }

  public String prettyFormat() {
    StringBuilder childrenStr = new StringBuilder();
    children.forEach(child -> {
      String human = child.getClass().toString().split("\\.")[2];
      childrenStr.append("\n\t\t\t\t").append(Objects.equals(human, "Man") ? "boy: " : "girl: ").append(child.prettyFormat());
    });
    StringBuilder petsStr = new StringBuilder();
    pets.forEach(pet -> petsStr.append(pet.prettyFormat()));
    return "family: \n" +
        "\t\tmother: " + mother.prettyFormat() + "\n" +
        "\t\tfather: " + father.prettyFormat() + "\n" +
        "\t\tchildren: " + (children.size() > 0 ?  childrenStr.toString() : "") + "\n" +
        "\t\tpets: " + petsStr.toString();
  }

  @Override
  public String toString() {
    return "Family{" +
        "mother=" + mother +
        ", father=" + father +
        ", children=" + children.toString() +
        ", pets: " + pets.toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Family family = (Family) o;
    return Objects.equals(this.mother, family.mother) &&
        Objects.equals(this.father, family.father) &&
        Objects.equals(this.children, family.children);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(mother, father);
    result = 31 * result + Objects.hashCode(children);
    return result;
  }

  @Override
  public void finalize() {
    System.out.println("Family has deleted");
  }

  @Override
  public Human bornChild() {
    boolean random = Math.random() < 0.5;
    String fatherSurname = this.father.getSurname();
    String name = random ? Names.WOMAN.getRandomName() : Names.MAN.getRandomName();
    int iq = (this.father.getIQ() + this.mother.getIQ()) / 2;
    Human newHuman = random ? new Woman(name, fatherSurname, "30/07/2021", iq, this) : new Man(name, fatherSurname, "30/07/2021", iq, this);
    addChild(newHuman);
    return newHuman;
  }

  @Override
  public Human bornChild(String manName, String womanName) {
    boolean random = Math.random() < 0.5;
    String fatherSurname = this.father.getSurname();
    String name = random ? womanName : manName;
    int iq = (this.father.getIQ() + this.mother.getIQ()) / 2;
    Human newHuman = random ? new Woman(name, fatherSurname, "30/07/2021", iq, this) : new Man(name, fatherSurname, "30/07/2021", iq, this);
    addChild(newHuman);
    return newHuman;
  }
}
