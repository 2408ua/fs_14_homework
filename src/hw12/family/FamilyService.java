package hw12.family;

import hw12.human.Human;
import hw12.pet.Pet;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class FamilyService {
  private final FamilyDao db;
  private final String dbFilePath = "src/hw12/db/families.data";

  public FamilyService() {
    this.db = new CollectionFamilyDao();
  }

  public ArrayList<Family> getAllFamilies() {
    return this.db.getAllFamilies();
  }

  public void printFamiliesInfo(ArrayList<Family> families) {
    List<String> collect = IntStream.range(0, families.size())
        .mapToObj(index -> (index + 1) + " - " + families.get(index).prettyFormat())
        .collect(Collectors.toList());

    collect.forEach(System.out::println);
  }

  public void displayAllFamilies() {
    this.printFamiliesInfo(this.getAllFamilies());
  }

  public ArrayList<Family> getFamiliesBiggerThan(int num) {
    return (ArrayList<Family>) this.db.getAllFamilies().stream()
        .filter(f -> f.countFamily() > num)
        .collect(Collectors.toList());
  }

  public ArrayList<Family> getFamiliesLessThan(int num) {
    return (ArrayList<Family>) this.db.getAllFamilies().stream()
        .filter(f -> f.countFamily() < num)
        .collect(Collectors.toList());
  }

  public int countFamiliesWithMemberNumber(int num) {
    ArrayList<Family> familyList = (ArrayList<Family>) this.db.getAllFamilies().stream()
        .filter(f -> f.countFamily() == num)
        .collect(Collectors.toList());
    return familyList.size();
  }

  public int createNewFamily(Human mother, Human father) {
    Family family = new Family(mother, father);
    return this.db.saveFamily(family);
  }

  public boolean deleteFamilyByIndex(int index) {
    return this.db.deleteFamily(index);
  }

  public boolean deleteFamilyByValue(Family family) {
    return this.db.deleteFamily(family);
  }

  public boolean bornChild(Family family, String manName, String womanName) {
    try {
      if (this.db.isFamilyNotExist(family)) return false;

      family.bornChild(manName, womanName);
      return true;

    } catch (Exception e) {
      System.out.println(e);
      return false;
    }
  }

  public Family adoptChild(Family family, Human human) {
    if (this.db.isFamilyNotExist(family)) return null;

    family.addChild(human);
    return family;
  }

  public void deleteAllChildrenOlderThen(int age) {
    ArrayList<Human> childrenTmp = new ArrayList<Human>();
    this.db.getAllFamilies()
        .forEach(f -> {
          f.getChildren().forEach(child -> {
            long birthDate = child.getBirthDate();
            LocalDate birthday = Instant.ofEpochMilli(birthDate).atZone(ZoneId.systemDefault()).toLocalDate();
            int year = birthday.getYear();
            if ((2021 - year) > age) {
              childrenTmp.add(child);
            }
          });
          childrenTmp.forEach(f::deleteChild);
          childrenTmp.clear();
        });
  }

  public void writeFamiliesListToFile(ArrayList<Family> families) {
    try {
      FileOutputStream fos = new FileOutputStream(dbFilePath);
      ObjectOutputStream oos = new ObjectOutputStream(fos);
      oos.writeObject(families);
      oos.close();
      System.out.println("List of families has been saved");

    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void loadFamiliesListFromFile() {
    ArrayList<Family> families = new ArrayList<Family>();
    try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(dbFilePath))) {

      families = ((ArrayList<Family>) ois.readObject());
      this.db.loadData(families);
      System.out.println("List of families has been loaded");

    } catch (Exception ex) {

      System.out.println(ex.getMessage());
    }
  }

  public int count() {
    return this.db.getAllFamilies().size();
  }

  public Family getFamilyById(int id) {
    return this.db.getFamilyByIndex(id);
  }

  public HashSet<Pet> getPets(int familyIndex) {
    if (this.db.isFamilyNotExist(familyIndex)) return null;

    return this.getFamilyById(familyIndex).getPets();
  }

  public boolean addPet(int familyIndex, Pet pet) {
    if (this.db.isFamilyNotExist(familyIndex)) return false;

    this.getFamilyById(familyIndex).addPet(pet);
    return true;
  }

  public boolean clearDb() {
    return this.db.clear();
  }
}
