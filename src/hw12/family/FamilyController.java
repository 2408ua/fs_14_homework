package hw12.family;

import hw12.Console;
import hw12.FamilyOverflowException;
import hw12.human.Human;
import hw12.human.Man;
import hw12.human.Woman;
import hw12.pet.Dog;
import hw12.pet.Pet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class FamilyController {
  private final Console console;
  private final FamilyService service;

  public FamilyController() {
    this.console = new Console();
    this.service = new FamilyService();
  }

  public ArrayList<Family> getAllFamilies() {
    return this.service.getAllFamilies();
  }

  public void createTestsFamilies() {
    this.service.clearDb();
    Dog pet = new Dog("Boy", 5, 49);

    Man father01 = new Man("Bob", "Marley", "23/08/1950", 90);
    Woman mother01 = new Woman("Mandy", "Marley", "11/11/1955", 100);

    Man father02 = new Man("Jon", "Black", "12/06/1993", 90);
    Woman mother02 = new Woman("Mandy", "Black", "14/05/1996", 100);

    Man father03 = new Man("Alex", "Cantor", "04/12/1983", 90);
    Woman mother03 = new Woman("April", "Cantor", "15/10/1980", 100);

    Man father04 = new Man("Charly", "Paterson", "23/11/1950", 90);
    Woman mother04 = new Woman("Agata", "Paterson", "01/01/1955", 100);

    Man father05 = new Man("Tri", "Bourne", "12/04/1975", 90);
    Woman mother05 = new Woman("Sara", "Bourne", "23/7/1985", 100);

    Man sun = new Man("Jeremy", "Ross", "26/08/1979", 95);
    Woman daughter = new Woman("Barbara", "Ross", "30/11/1990", 95);

    int index = this.service.createNewFamily(mother01, father01);
    int index2 = this.service.createNewFamily(mother02, father02);
    int index3 = this.service.createNewFamily(mother03, father03);
    int index4 = this.service.createNewFamily(mother04, father04);
    int index5 = this.service.createNewFamily(mother05, father05);

    Family family01 = this.getFamilyById(index);
    Family family02 = this.getFamilyById(index2);
    Family family03 = this.getFamilyById(index3);
    family01.addPet(pet);
    this.service.bornChild(family01, "Cassy", "April");
    this.service.adoptChild(family02, sun);
    this.service.adoptChild(family03, daughter);
    this.service.addPet(index3, pet);
    System.out.println("Test data has been created.");
  }

  public void displayAllFamilies() {
    this.service.displayAllFamilies();
  }

  public void getFamiliesBiggerThan() {
    int answer = this.console.getFamilyCount();
    ArrayList<Family> families = this.service.getFamiliesBiggerThan(answer);
    this.service.printFamiliesInfo(families);
  }

  public void getFamiliesLessThan() {
    int answer = this.console.getFamilyCount();
    ArrayList<Family> families = this.service.getFamiliesLessThan(answer);
    this.service.printFamiliesInfo(families);
  }

  public void countFamiliesWithMemberNumber() {
    int answer = this.console.getFamilyCount();
    int count = this.service.countFamiliesWithMemberNumber(answer);
    System.out.println("Count families = " + count);
  }

  public void createNewFamily() {
    try {
      System.out.println("Enter mother data:");
      HashMap<String, String> motherData = this.console.getHumanData();
      System.out.println("\nEnter father data:");
      HashMap<String, String> fatherData = this.console.getHumanData();
      String motherBirthDate = motherData.get("day") + "/" + motherData.get("month") + "/" + motherData.get("year");
      String fatherBirthDate = fatherData.get("day") + "/" + fatherData.get("month") + "/" + fatherData.get("year");

      Woman mother = new Woman(motherData.get("name"), motherData.get("surname"), motherBirthDate,
          Integer.parseInt(motherData.get("iq")));
      Man father = new Man(fatherData.get("name"), fatherData.get("surname"), fatherBirthDate,
          Integer.parseInt(fatherData.get("iq")));
      this.service.createNewFamily(mother, father);
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  public void deleteFamilyByIndex() {
    int dbSize = this.service.getAllFamilies().size();
    int index = this.console.getFamilyIndex(dbSize);
    this.service.deleteFamilyByIndex(index - 1);
  }

  public boolean deleteFamilyByValue(Family family) {
    return this.service.deleteFamilyByValue(family);
  }

  public void bornChild() {
    int dbSize = this.service.getAllFamilies().size();
    int index = this.console.getFamilyIndex(dbSize);
    HashMap<String, String> childData = this.console.getChildData();
    Family family = this.service.getFamilyById(index);
    int familySize = family.countFamily();
    if (familySize == 5) throw new FamilyOverflowException();
    family.bornChild(childData.get("boyName"), childData.get("girlName"));
  }

  public void adoptChild() {
    int dbSize = this.service.getAllFamilies().size();
    int index = this.console.getFamilyIndex(dbSize);
    Family family = this.service.getFamilyById(index);
    int familySize = family.countFamily();
    if (familySize == 5) throw new FamilyOverflowException();
    System.out.println("Enter adopt child data:");
    HashMap<String, String> childData = this.console.getHumanData();
    String childBirthDate = childData.get("day") + "/" + childData.get("month") + "/" + childData.get("year");

    Human child = new Human(childData.get("name"), childData.get("surname"), childBirthDate,
        Integer.parseInt(childData.get("iq")));
    this.service.adoptChild(family, child);
  }

  public void deleteAllChildrenOlderThen() {
    int age = this.console.getAge();
    this.service.deleteAllChildrenOlderThen(age);
  }

  public void writeFamiliesListToFile() {
    ArrayList<Family> families = this.service.getAllFamilies();
    this.service.writeFamiliesListToFile(families);
  }

  public void loadFamiliesListFromFile() {
    this.service.loadFamiliesListFromFile();
  }

  public int count() {
    return this.service.count();
  }

  public Family getFamilyById(int num) {
    return this.service.getFamilyById(num);
  }

  public HashSet<Pet> getPets(int familyIndex) {
    return this.service.getPets(familyIndex);
  }

  public boolean addPet(int familyIndex, Pet pet) {
    return this.service.addPet(familyIndex, pet);
  }
}
