package hw12.family;

import java.util.*;

public interface FamilyDao {

  public void loadData(ArrayList<Family> families);

  public ArrayList<Family> getAllFamilies();

  public Family getFamilyByIndex(int index);

  public Family getFamilyByValue(Family family);

  public boolean isFamilyNotExist(Family family);

  public boolean isFamilyNotExist(int index);

  public boolean deleteFamily(int index);

  public boolean deleteFamily(Family family);

  public int saveFamily(Family family);

  public boolean clear();
}
