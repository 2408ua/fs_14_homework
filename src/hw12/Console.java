package hw12;

import java.util.HashMap;
import java.util.Scanner;

public class Console {
  private final Scanner scan;
  int mainMenuSize;
  int editMenuSize;

  public Console() {
    this.scan = new Scanner(System.in);
    Menu menu = new Menu();
    this.mainMenuSize = menu.getMainMenuSize();
    this.editMenuSize = menu.getEditMenuSize();
  }

  private boolean isNotNumeric(String str) {
    try {
      Integer.parseInt(str);
      return false;
    } catch (Exception e) {
      return true;
    }
  }

  private String inputData(String str) {
    System.out.print(str);
    return scan.nextLine();
  }

  private boolean isNotCorrectMenuNumber(String n) {
    if (isNotNumeric(n)) return true;
    return Integer.parseInt(n) < 1 || Integer.parseInt(n) > this.mainMenuSize;
  }

  private boolean isNotCorrectFamilyCount(String n) {
    if (isNotNumeric(n)) return true;
    return Integer.parseInt(n) < 2 || Integer.parseInt(n) > 30;
  }

  public int getMenuNumber() {
    String answer = inputData("Enter menu number - ");

    while (isNotCorrectMenuNumber(answer)) {
      answer = inputData("Enter correct number - ");
    }
    return Integer.parseInt(answer);
  }

  public int getFamilyCount() {
    String answer = inputData("Enter family count - ");

    while (isNotCorrectFamilyCount(answer)) {
      answer = inputData("Enter correct family count - ");
    }
    return Integer.parseInt(answer);
  }

  public HashMap<String, String> getHumanData() {
    String name = inputData("Enter name - ");
    String surname = inputData("Enter surname - ");
    String year = inputData("Enter birth year - ");
    String month = inputData("Enter birth month - ");
    String day = inputData("Enter birth day - ");
    String iq = inputData("Enter IQ - ");
    HashMap<String, String> tmp = new HashMap<String, String>();
    tmp.put("name", name);
    tmp.put("surname", surname);
    tmp.put("year", year);
    tmp.put("month", month);
    tmp.put("day", day);
    tmp.put("iq", iq);
    return tmp;
  }

  public HashMap<String, String> getChildData() {
    String boyName = inputData("Enter boy name - ");
    String girlName = inputData("Enter girl name - ");
    HashMap<String, String> tmp = new HashMap<String, String>();
    tmp.put("boyName", boyName);
    tmp.put("girlName", girlName);
    return tmp;
  }

  public int getFamilyIndex(int dbSize) {
    String answer = inputData("Enter family index - ");

    while (isNotNumeric(answer) || Integer.parseInt(answer) < 1 || Integer.parseInt(answer) > dbSize) {
      answer = inputData("Enter correct family index - ");
    }
    return Integer.parseInt(answer) - 1;
  }

  public int getAge() {
    String answer = inputData("Enter age - ");

    while (isNotNumeric(answer)) {
      answer = inputData("Enter correct age - ");
    }
    return Integer.parseInt(answer);
  }
}
