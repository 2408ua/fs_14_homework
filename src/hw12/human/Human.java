package hw12.human;

import hw12.family.Family;
import hw12.pet.Pet;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;

public class Human implements Serializable {
  private String name;
  private String surname;
  private long birthDate;
  private int IQ;
  private HashMap<String, String> schedule;
  private Family family = null;


  Human() {
  }

  public Human(String name, String surname, String birthDate, int iq, Family family) {
    this(name, surname, birthDate, iq, new HashMap<String, String>(), family);
  }

  public Human(String name, String surname, String birthDate) {
    this(name, surname, birthDate, 90, new HashMap<String, String>());
  }

  public Human(String name, String surname, String birthDate, int iq) {
    this(name, surname, birthDate, iq, new HashMap<String, String>());
  }

  public Human(String name, String surname, String birthDate, int IQ, HashMap<String, String> schedule) {
    this.name = name;
    this.surname = surname;
    this.birthDate = getTime(birthDate);
    this.IQ = IQ;
    this.schedule = schedule;
  }

  public Human(String name, String surname, String birthDate, int IQ, HashMap<String, String> schedule, Family family) {
    this.name = name;
    this.surname = surname;
    this.birthDate = getTime(birthDate);
    this.IQ = IQ;
    this.schedule = schedule;
    this.family = family;
  }

  private long getTime(String date) {
    try {
      DateFormat df = new SimpleDateFormat("dd/MM/yyy");
      return df.parse(date).getTime();

    } catch (Exception e) {
      System.out.println(e);
      return -1;
    }
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public long getBirthDate() {
    return birthDate;
  }

  public String getStringBirthDate() {
    LocalDate birthday = Instant.ofEpochMilli(this.birthDate).atZone(ZoneId.systemDefault()).toLocalDate();
    int year = birthday.getYear();
    int month = birthday.getMonthValue();
    int dayOfMonth = birthday.getDayOfMonth();
    return dayOfMonth + "/" + month + "/" + year;
  }

  public int getIQ() {
    return IQ;
  }

  public Family getFamily() {
    return this.family;
  }

  public String describeAge() {
    LocalDate today = LocalDate.now();
    LocalDate birthday = Instant.ofEpochMilli(this.birthDate).atZone(ZoneId.systemDefault()).toLocalDate();
    Period p = Period.between(birthday, today);
    return "You are " + p.getYears() + " years, " + p.getMonths() +
        " months, and " + p.getDays() + " days";
  }

  public void setFamily(Family family) {
    this.family = family;
  }

  private HashMap<String, Integer> parseDate(String str) {
    String[] arr = str.split("/");
    HashMap<String, Integer> tmp = new HashMap<String, Integer>();
    tmp.put("year", Integer.parseInt(arr[2]));
    tmp.put("month", Integer.parseInt(arr[1]));
    tmp.put("day", Integer.parseInt(arr[0]));

    return tmp;
  }

  public boolean feedPet(boolean isFeedTime, String nikname) {
    if (family != null) {
      HashSet<Pet> pets = family.getPets();
      for (Pet p : pets) {
        if (p.getNikname().equals(nikname)) {
          if (isFeedTime || p.getTrickLevel() > Math.random() * 100) {
            System.out.printf("Хм... покормлю ка я %s.\n", p.getNikname());
            return true;
          }
          System.out.printf("Думаю, %s не голоден.\n", p.getNikname());
          return false;
        }
      }
    }
    return false;
  }

  public void describePets() {
    if (family != null) {
      HashSet<Pet> pets = family.getPets();
      for (Pet p : pets) {
        System.out.printf("У меня есть %s, ему %d лет, он %s\n", p.getSpecies(), p.getAge(), p.getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый");
      }
    }
    System.out.println("No family, no pet!");
  }

  public String prettyFormat(){
    return String.format("{name=%s, surname=%s, birthDate=%s, iq=%d, schedule=%s}", name, surname, this.getStringBirthDate(), IQ, schedule.toString());
  }

  public String toString() {
    return String.format("Human{name=%s, surname=%s, birthDate=%s, iq=%d, schedule=%s}", name, surname, this.getStringBirthDate(), IQ, schedule.toString());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Human human = (Human) o;
    return birthDate == human.birthDate &&
        IQ == human.IQ
        && Objects.equals(name, human.name)
        && Objects.equals(surname, human.surname)
        && schedule.equals(human.schedule);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(name, surname, birthDate, IQ);
    return 31 * result + Objects.hashCode(schedule);
  }

  @Override
  public void finalize() {
    System.out.println("Human has deleted");
  }
}
