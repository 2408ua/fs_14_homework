package hw12.human;

import hw12.family.Family;

import java.util.HashMap;

public final class Man extends Human {
  public Man(String name, String surname, String birthDate, int IQ) {
    super(name, surname, birthDate, IQ);
  }

  public Man(String name, String surname, String birthDate, int IQ, HashMap<String, String> schedule) {
    super(name, surname, birthDate, IQ, schedule);
  }

  public Man(String name, String surname, String birthDate, int iq, Family family) {
    super(name, surname, birthDate, iq, family);
  }

  public Man(String name, String surname, String birthDate) {
    super(name, surname, birthDate);
  }

  public Man() {
  }

  public void repairCar() {
    System.out.println("Я чиню машину, меня не беспокоить!");
  }

  public void greetPet() {
    System.out.println("Привет, зверьё моё!");
  }
}
