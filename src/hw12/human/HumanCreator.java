package hw12.human;

public interface HumanCreator {

  public Human bornChild();
  public Human bornChild(String manName, String womanName);
}
