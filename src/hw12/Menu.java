package hw12;

import java.util.ArrayList;

public class Menu {
  ArrayList<String> main;
  ArrayList<String> editFamily;

  public Menu() {
    this.main = new ArrayList<String>();
    this.editFamily = new ArrayList<String>();
    this.main.add("- 1. Заполнить тестовыми данными");
    this.main.add("- 2. Отобразить весь список семей");
    this.main.add("- 3. Отобразить список семей, где количество людей больше заданного");
    this.main.add("- 4. Отобразить список семей, где количество людей меньше заданного");
    this.main.add("- 5. Подсчитать количество семей, где количество членов равно");
    this.main.add("- 6. Создать новую семью");
    this.main.add("- 7. Удалить семью по индексу семьи в общем списке");
    this.main.add("- 8. Редактировать семью по индексу семьи в общем списке");
    this.main.add("- 9. Удалить всех детей старше возраста");
    this.main.add("- 10. Сохранить список семей");
    this.main.add("- 11. Загрузить список семей");
    this.main.add("- 12. Выход");

    this.editFamily.add("- 1. Родить ребенка");
    this.editFamily.add("- 2. Усыновить ребенка");
    this.editFamily.add("- 3. Вернуться в главное меню");
  }

  public void printMainMenu() {
    System.out.println("************************************************************");
    this.main.forEach(System.out::println);
    System.out.println("*************************** Main Menu **********************");
  }

  public void printEditFamilyMenu() {
    System.out.println("************************************************************");
    this.editFamily.forEach(System.out::println);
    System.out.println("*************************** Edit Family Menu ***************");
  }

  public int getMainMenuSize(){
    return this.main.size();
  }
  public int getEditMenuSize(){
    return this.editFamily.size();
  }
}
