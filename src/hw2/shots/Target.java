package hw2.shots;

public class Target extends Field {
  private final int targetLength;
  private int targetHitLength = 0;
  private final int[][] targetCoordinates;

  public Target(int row, int coll, int targetLength) {
    super(row, coll);
    this.targetLength = targetLength;
    targetCoordinates = getRandomCoordinates();
  }

  public int[][] getTarget() {
    return targetCoordinates;
  }

  public boolean isTargetKilled() {
    return targetHitLength == targetLength;
  }

  public void targetHit(int row, int coll) {
    for (int[] item : targetCoordinates) {
      if (item[0] == row && item[1] == coll) ++targetHitLength;
    }
  }

  private int getRandomNumber(int to) {
    final double r = Math.random();
    double result = r * to + 1;
    return (int) result;
  }

  /**
   * 0 - row
   * 1 - coll
   */
  private int getDirection() {
    return Math.random() < 0.5 ? 0 : 1;
  }

  private int[][] getRandomCoordinates() {
    final int row = getRow();
    final int coll = getColl();
    final int targetRow = getRandomNumber(row);
    final int targetColl = getRandomNumber(coll);
    final int direction = getDirection();
    final int[][] result = new int[targetLength][];

    for (int i = 0; i < targetLength; i++) {
      result[i] = new int[2];
      int q = i;
      if (direction == 0) {
        if (targetRow > row - (targetLength - 1) && i != 0) {
          q = -i;
        }
        result[i][0] = targetRow + q;
        result[i][1] = targetColl;
      } else {
        if (targetColl > coll - (targetLength - 1) && i != 0) {
          q = -i;
        }
        result[i][0] = targetRow;
        result[i][1] = targetColl + q;
      }
    }
    return result;
  }
}
