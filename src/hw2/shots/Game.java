package hw2.shots;

import java.util.Scanner;

public class Game extends Shot {
  private final Scanner scan;

  public Game(int fieldRow, int fieldColl, int targetLength) {
    super(fieldRow, fieldColl, targetLength);
    scan = new Scanner(System.in);
  }

  private String inputData(String str) {
    print(str);
    return scan.nextLine();
  }

  private void printField() {
    final int[][] shots = getShots();
    final int[][] target = getTarget();

    getField(target, shots);
  }

  public void run() {
    boolean game = true;
    System.out.println("All set. Get ready to rumble!");

    while (game) {
      printField();
      String rowStr = "Input shot row: ";
      String shotRow = inputData(rowStr);

      while (isNotCorrectRow(shotRow)) {
        print("Input correct row!\n");
        shotRow = inputData(rowStr);
      }

      String collStr = "Input shot coll: ";
      String shotColl = inputData(collStr);

      while (isNotCorrectColl(shotColl)) {
        print("Input correct coll!\n");
        shotColl = inputData(collStr);
      }

      makeShot(shotRow, shotColl);

      if (isTargetKilled()) {
        printField();
        print("You have won!\n");
        game = false;
      }
    }
  }
}
