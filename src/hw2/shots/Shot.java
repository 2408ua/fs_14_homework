package hw2.shots;

public class Shot extends Target {
  private int[][] shots = new int[0][];

  public Shot(int row, int coll, int targetLength) {
    super(row, coll, targetLength);
  }

  public void makeShot(String row, String coll) {
    final int r = Integer.parseInt(row);
    final int c = Integer.parseInt(coll);
    int[][] tmp = new int[shots.length + 1][];
    tmp[shots.length] = new int[]{r, c};
    System.arraycopy(shots, 0, tmp, 0, shots.length);
    if (isNewShot(r, c)) targetHit(r, c);
    shots = tmp;
  }

  public int[][] getShots() {
    return shots;
  }

  private boolean isNewShot(int row, int coll) {
    for (int[] item : shots) {
      if (item[0] == row && item[1] == coll) return false;
    }
    return true;
  }
}
