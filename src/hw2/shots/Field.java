package hw2.shots;

public class Field {
  private final int row;
  private final int coll;

  public Field(int row, int coll) {
    this.row = row;
    this.coll = coll;
  }

  public int getRow() {
    return row;
  }

  public int getColl() {
    return coll;
  }

  private boolean isNotNumeric(String str) {
    try {
      Integer.parseInt(str);
      return false;
    } catch (Exception e) {
      return true;
    }
  }

  public boolean isNotCorrectRow(String row) {
    if (isNotNumeric(row)) return true;

    final int r = Integer.parseInt(row);
    return r < 1 || r > this.row;
  }

  public boolean isNotCorrectColl(String coll) {
    if (isNotNumeric(coll)) return true;

    final int c = Integer.parseInt(coll);
    return c < 1 || c > this.coll;
  }

  public void print(String str) {
    System.out.print(str);
  }

  private Boolean isLeftRightBorder(int c) {
    return c == -1 || c == coll + 1;
  }

  private Boolean isRowNumbers(int c) {
    return c == 0;
  }

  private Boolean isCollNumbers(int r) {
    return r == 0;
  }

  private String getFieldSymbol(int IndexRow, int IndexColl, int[][] target, int[][] shots) {
    String result = " - |";
    for (int[] shot : shots) {
      if (shot[0] == IndexRow && shot[1] == IndexColl) {
        result = " * |";
        for (int[] t : target) {
          if (shot[0] == t[0] && shot[1] == t[1]) {
            result = " x |";
            break;
          }
        }
        break;
      }
    }
    return result;
  }

  public void getField(int[][] target, int[][] shots) {
    for (int r = 0; r <= row; r++) {
      for (int c = -1; c <= coll + 1; c++) {
        if (isLeftRightBorder(c)) {
          print("");
        } else if (isRowNumbers(c)) {
          print(r + " |");
        } else if (isCollNumbers(r)) {
          print(" " + c + " |");
        } else {
          print(getFieldSymbol(r, c, target, shots));
        }
      }
      print("\n");
    }
  }
}
