package hw11;

import java.util.HashMap;

public class App {
  private final Console console;
  private final Menu menu;
  HashMap<Integer, Runnable> mainRouts;
  HashMap<Integer, Runnable> editRouts;

  public App() {
    this.console = new Console();
    this.menu = new Menu();
    Router router = new Router();
    this.mainRouts = router.getMainRouter();
    this.editRouts = router.getEditRouter();
  }

  public void run() {
    while (true) {
      this.menu.printMainMenu();
      int menuNumber = this.console.getMenuNumber();
      if (menuNumber == 8) editFamily();
      if (menuNumber == 10) break;
      if (menuNumber != 8) this.mainRouts.get(menuNumber).run();
    }
  }

  private void editFamily() {
    while (true) {
      this.menu.printEditFamilyMenu();
      int editMenuNumber = this.console.getMenuNumber();
      if (editMenuNumber == 3) break;
      this.editRouts.get(editMenuNumber).run();
    }
  }
}
