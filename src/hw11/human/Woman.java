package hw11.human;

import hw11.family.Family;

import java.util.HashMap;

public final class Woman extends Human {
  public Woman(String name, String surname, String birthDate, int IQ) {
    super(name, surname, birthDate, IQ);
  }

  public Woman(String name, String surname, String birthDate, int IQ, HashMap<String, String> schedule) {
    super(name, surname, birthDate, IQ, schedule);
  }

  public Woman(String name, String surname, String birthDate, int iq, Family family) {
    super(name, surname, birthDate, iq, family);
  }

  public Woman(String name, String surname, String birthDate) {
    super(name, surname, birthDate);
  }

  public Woman() {
  }

  public void makeUp() {
    System.out.println("Я крашусь, буду через 5 мин.");
  }

  public void greetPet() {
    System.out.println("Привет, зверьё моё!");
  }
}
