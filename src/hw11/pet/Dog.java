package hw11.pet;

import java.util.HashSet;

public class Dog extends Pet implements Foul {

  public Dog(String nikname, int age, int trickLevel, HashSet<String> habits) {
    super(Species.DOG, nikname, age, trickLevel, habits);
  }

  public Dog(String nikname, int age, int trickLevel) {
    super(Species.DOG, nikname, age, trickLevel);
  }


  @Override
  public void respond() {
    System.out.printf("Гав. Привет, хозяин. Я - %s. Я соскучился!", this.getNikname());
  }

  @Override
  public void foul() {
    System.out.print("Нужно хорошо замести следы...");
  }
}
