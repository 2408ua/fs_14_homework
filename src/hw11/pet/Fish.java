package hw11.pet;

import java.util.HashSet;

public class Fish extends Pet {
  public Fish(String nikname, int age, int trickLevel,  HashSet<String> habits) {
    super(Species.FISH, nikname, age, trickLevel, habits);
  }

  @Override
  public void respond() {
    System.out.printf("Я - %s.", this.getNikname());
  }
}
