package hw11.pet;

import java.util.HashSet;

public class DomesticCat extends Pet implements Foul {
  public DomesticCat( String nikname, int age, int trickLevel,  HashSet<String> habits) {
    super(Species.DOMESTIC_CAT, nikname, age, trickLevel, habits);
  }

  @Override
  public void respond() {
    System.out.printf("Мяу. Привет, хозяин. Я - %s. Я соскучился!", this.getNikname());
  }

  @Override
  public void foul() {
    System.out.print("Нужно хорошо замести следы...");
  }
}
