package hw11;

public class FamilyOverflowException extends RuntimeException {

  public FamilyOverflowException() {

    super("Family is too big. Maximal size is 5");
  }
}
