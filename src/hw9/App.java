package hw9;

import java.text.*;

public class App {
  public static void main(String[] args) throws ParseException {
    String str = "20/03/2016";
    long f = getTime(str);
    Human human = new Human("Alex", "Marley", f);
    String bd = human.describeAge();
    System.out.println(bd);
    System.out.println(human);
  }

  public static long getTime(String date) throws ParseException {
    DateFormat df = new SimpleDateFormat("dd/MM/yyy");
    return df.parse(date).getTime();
  }
}
