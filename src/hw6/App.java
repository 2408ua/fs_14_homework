package hw6;

import hw6.human.Human;
import hw6.human.*;
import hw6.pet.*;


public class App {


  public static void main(String[] args) {
    Dog pet01 = new Dog("Boy", 5, 49, new String[]{"run", "swimming", "sleep"});
    Man father01 = new Man("Bob", "Marley", 1950, 90);
    Woman mother01 = new Woman("Mandy", "Marley", 1955, 100);
    Man sun01 = new Man("Alex", "Marley", 1979, 95,
        new String[][]{{DayOfWeek.MONDAY.name(), ToDo.SWIMMING.name()},
        {DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name()},
        {DayOfWeek.FRIDAY.name(), ToDo.FOOTBALL.name()}});
    Woman daughter01 = new Woman("Barbara", "Marley", 1990, 95,
        new String[][]{{DayOfWeek.MONDAY.name(), ToDo.DANCING.name()},
            {DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name()},
            {DayOfWeek.FRIDAY.name(), ToDo.SINGING.name()}});

    Family family01 = new Family(mother01, father01);
    family01.addChild(sun01);
    family01.addChild(daughter01);
    family01.addPet(pet01);

    System.out.println("-----------------");
    System.out.println(family01.toString());
    System.out.println("Family count - " + family01.countFamily());
    System.out.println("-----------------");

    Human newChild = family01.bornChild();
    System.out.println(newChild.toString());

    System.out.println("-----------------");
    System.out.println(family01.toString());
    System.out.println("Family count - " + family01.countFamily());
    System.out.println("-----------------");

    father01.repairCar();
    mother01.makeUp();
    pet01.respond();
  }
}
