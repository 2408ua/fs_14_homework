package hw6;

import hw6.human.*;
import hw6.pet.Pet;

import java.util.Arrays;
import java.util.Objects;

public class Family implements HumanCreator {
  private Pet pet;
  private final Woman mother;
  private final Man father;
  private Human[] children;


  static {
    System.out.println("Class Family is loading.");
  }

  {
    System.out.println("Family: new instance.");
  }

  public Family(Woman mother, Man father) {
    this.mother = mother;
    this.father = father;
    this.mother.setFamily(this);
    this.father.setFamily(this);
    this.children = new Human[0];
  }

  public Pet getPet() {
    return this.pet;
  }

  public Woman getMother() {
    return this.mother;
  }

  public Man getFather() {
    return this.father;
  }

  public Human[] getChildren() {
    return this.children;
  }

  public void addChild(Human child) {
    int length = this.children.length + 1;
    Human[] tmp = new Human[length];
    System.arraycopy(this.children, 0, tmp, 0, this.children.length);
    tmp[length - 1] = child;
    this.children = tmp;
    this.children[length - 1].setFamily(this);
  }

  public void addPet(Pet pet) {
    this.pet = (Pet) pet;
    pet.setFamily(this);
  }

  public void deletePet(Pet pet) {
    if (this.pet == pet) {
      pet.setFamily(null);
    }
  }

  public boolean deleteChild(int index) {
    int length = this.children.length;
    if (length == 0 || length - 1 < index) return false;
    this.children[index].setFamily(null);

    Human[] tmp = new Human[length - 1];
    for (int i = 0; i < length - 1; i++) {
      if (i < index) {
        tmp[i] = this.children[i];
        continue;
      }
      tmp[i] = this.children[i + 1];
    }
    this.children = tmp;
    return true;
  }

  public int countFamily() {
    return this.children.length + 2;
  }

  public int getCountChildren() {
    return this.children.length;
  }

  private boolean isNotChild(Human child) {
    for (Human item : this.children) {
      if (item.equals(child)) return false;
    }
    return true;
  }

  public boolean deleteChild(Human child) {
    if (children.length == 0) return false;
    if (this.isNotChild(child)) return false;

    child.setFamily(null);

    Human[] res = new Human[children.length - 1];
    int count = 0;
    for (Human item : this.children) {
      if (item.equals(child)) {
        continue;
      }
      res[count] = item;
      count++;
    }
    this.children = res;
    return true;
  }

  @Override
  public String toString() {
    return "Family{" +
        "pet=" + pet +
        ", mother=" + mother +
        ", father=" + father +
        ", children=" + Arrays.toString(children) +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Family family = (Family) o;
    return Objects.equals(this.mother, family.mother) &&
        Objects.equals(this.father, family.father) &&
        Arrays.equals(this.children, family.children);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(mother, father);
    result = 31 * result + Arrays.hashCode(children);
    return result;
  }

  @Override
  public void finalize() {
    System.out.println("Family has deleted");
  }

  @Override
  public Human bornChild() {
    boolean random = Math.random() < 0.5;
    String fatherSurname = this.father.getSurname();
    String name = random ? Names.WOMAN.getRandomName() : Names.MAN.getRandomName();
    int iq = (this.father.getIQ() + this.mother.getIQ()) / 2;
    Human newHuman = random ? new Woman(name, fatherSurname, 0, iq, this) : new Man(name, fatherSurname, 0, iq, this);
    addChild(newHuman);
    return newHuman;
  }
}
