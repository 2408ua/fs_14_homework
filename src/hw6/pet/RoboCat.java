package hw6.pet;

public class RoboCat extends Pet implements Foul {
  private final Species species;

  public RoboCat( String nikname, int age, int trickLevel, String[] habits) {
    super(nikname, age, trickLevel, habits);
    this.species = Species.ROBO_CAT;
  }

  @Override
  public void respond() {
    System.out.printf("Мяу. Привет, хозяин. Я - %s. Я соскучился!", this.getNikname());
  }

  @Override
  public void foul() {
    System.out.print("Нужно хорошо замести следы...");
  }
}
