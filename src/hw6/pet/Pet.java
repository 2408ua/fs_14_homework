package hw6.pet;

import hw6.Family;

public abstract class Pet {
  private final Species species = Species.UNKNOWN;
  private final String nikname;
  private final int age;
  private final int trickLevel;
  private final String[] habits;
  private Family family = null;

  public Pet(String nikname, int age, int trickLevel, String[] habits) {
    this.nikname = nikname;
    this.age = age;
    this.trickLevel = trickLevel;
    this.habits = habits;
  }

  public Species getSpecies() {
    return species;
  }

  public Family getFamily() {
    return family;
  }

  public void setFamily(Family family) {
    this.family = family;
  }

  public String getNikname() {
    return nikname;
  }

  public int getAge() {
    return age;
  }

  public int getTrickLevel() {
    return trickLevel;
  }

  public String[] getHabits() {
    return habits;
  }

  public void eat() {
    System.out.print("Я кушаю!");
  }

  public abstract void respond();
}
