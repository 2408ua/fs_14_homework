package hw6.pet;

public class Fish extends Pet {
  private final Species species;

  public Fish(String nikname, int age, int trickLevel, String[] habits) {
    super(nikname, age, trickLevel, habits);
    this.species = Species.FISH;
  }

  @Override
  public void respond() {
    System.out.printf("Я - %s.", this.getNikname());
  }
}
