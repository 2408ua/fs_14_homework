package hw6.pet;

public class Dog extends Pet implements Foul {
  private final Species species;

  public Dog(String nikname, int age, int trickLevel, String[] habits) {
    super(nikname, age, trickLevel, habits);
    this.species = Species.DOG;
  }

  @Override
  public void respond() {
    System.out.printf("Гав. Привет, хозяин. Я - %s. Я соскучился!", this.getNikname());
  }

  @Override
  public void foul() {
    System.out.print("Нужно хорошо замести следы...");
  }
}
