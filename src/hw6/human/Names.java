package hw6.human;

public enum Names {
  MAN(new String[]{"Bob", "Jon", "Alex", "Charly"}),
  WOMAN(new String[]{"April", "Agata", "Cary", "Sarah"});
  private final String[] names;

  Names(String[] names) {
    this.names = names;
  }

  public String getRandomName() {
    int index = (int) (Math.random() * this.names.length);
    return this.names[index];
  }
}
