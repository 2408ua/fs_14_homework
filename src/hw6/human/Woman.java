package hw6.human;

import hw6.Family;

public final class Woman extends Human {
  public Woman(String name, String surname, int year, int IQ) {
    super(name, surname, year, IQ);
  }

  public Woman(String name, String surname, int year, int IQ, String[][] schedule) {
    super(name, surname, year, IQ, schedule);
  }

  public Woman(String name, String surname, int year, int iq, Family family) {
    super(name, surname, year, iq, family);
  }

  public Woman(String name, String surname, int year) {
    super(name, surname, year);
  }

  public Woman() {
  }

  public void makeUp() {
    System.out.println("Я крашусь, буду через 5 мин.");
  }

  public void greetPet() {
    System.out.println("Привет, зверьё моё!");
  }
}
