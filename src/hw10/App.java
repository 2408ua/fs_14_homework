package hw10;

import hw7.Family;
import hw7.human.Man;
import hw7.human.Woman;
import hw7.pet.Dog;

public class App {
  public static void main(String[] args) {
    FamilyService service = new FamilyService();
    Dog pet = new Dog("Boy", 5, 49);
    Man father01 = new Man("Bob", "Marley", 1950, 90);
    Woman mother01 = new Woman("Mandy", "Marley", 1955, 100);

    Man father02 = new Man("Jon", "Black", 1993, 90);
    Woman mother02 = new Woman("Mandy", "Black", 1996, 100);

    Man father03 = new Man("Alex", "Cantor", 1983, 90);
    Woman mother03 = new Woman("April", "Cantor", 1980, 100);

    Man father04 = new Man("Charly", "Paterson", 1950, 90);
    Woman mother04 = new Woman("Agata", "Paterson", 1955, 100);

    Man father05 = new Man("Tri", "Bourne", 1975, 90);
    Woman mother05 = new Woman("Sara", "Bourne", 1985, 100);

    Man sun = new Man("Jeremy", "Ross", 1979, 95);
    Woman daughter = new Woman("Barbara", "Ross", 1990, 95);

    int index = service.createNewFamily(mother01, father01);
    int index2 = service.createNewFamily(mother02, father02);
    int index3 = service.createNewFamily(mother03, father03);
    int index4 = service.createNewFamily(mother04, father04);
    int index5 = service.createNewFamily(mother05, father05);

    Family family01 = service.getFamilyById(index);
    Family family02 = service.getFamilyById(index2);
    Family family03 = service.getFamilyById(index3);
    service.bornChild(family01, "Cassy", "April");
    service.adoptChild(family02, sun);
    service.adoptChild(family03, daughter);
    service.addPet(index3, pet);

    printLine("displayAllFamilies");
    service.displayAllFamilies();

    printLine("getFamiliesBiggerThan");
    service.getFamiliesBiggerThan(2);

    printLine("getFamiliesLessThan");
    service.getFamiliesLessThan(3);

    printLine("countFamiliesWithMemberNumber");
    System.out.println(service.countFamiliesWithMemberNumber(3));

    printLine("deleteAllChildrenOlderThen");
    service.deleteAllChildrenOlderThen(21);
    service.displayAllFamilies();
  }

  public static void printLine(String name) {
    System.out.println("");
    System.out.print("############################################################################  ");
    System.out.print(name);
    System.out.println("  ############################################################################");
  }
}
