package hw10;

import hw7.Family;
import hw7.human.Human;
import hw7.pet.Pet;
import hw8.CollectionFamilyDao;
import hw8.FamilyDao;

import java.util.*;
import java.util.stream.*;


public class FamilyService {
  private final FamilyDao bd;

  public FamilyService() {
    this.bd = new CollectionFamilyDao();
  }

  public ArrayList<Family> getAllFamilies() {
    return this.bd.getAllFamilies();
  }

  private void printFamiliesInfo(ArrayList<Family> families) {
    List<String> collect = IntStream.range(0, families.size())
        .mapToObj(index -> (index + 1) + " - " + families.get(index))
        .collect(Collectors.toList());

    collect.forEach(System.out::println);
  }

  public void displayAllFamilies() {
    this.printFamiliesInfo(this.getAllFamilies());
  }

  public ArrayList<Family> getFamiliesBiggerThan(int num) {
    ArrayList<Family> familyList = (ArrayList<Family>) this.bd.getAllFamilies().stream()
        .filter(f -> f.countFamily() > num)
        .collect(Collectors.toList());
    this.printFamiliesInfo(familyList);
    return familyList;
  }

  public ArrayList<Family> getFamiliesLessThan(int num) {
    ArrayList<Family> familyList = (ArrayList<Family>) this.bd.getAllFamilies().stream()
        .filter(f -> f.countFamily() < num)
        .collect(Collectors.toList());
    this.printFamiliesInfo(familyList);
    return familyList;
  }

  public int countFamiliesWithMemberNumber(int num) {
    ArrayList<Family> familyList = (ArrayList<Family>) this.bd.getAllFamilies().stream()
        .filter(f -> f.countFamily() == num)
        .collect(Collectors.toList());
    return familyList.size();
  }

  public int createNewFamily(Human mother, Human father) {
    Family family = new Family(mother, father);
    return this.bd.saveFamily(family);
  }

  public boolean deleteFamilyByIndex(int index) {
    return this.bd.deleteFamily(index);
  }

  public boolean deleteFamilyByValue(Family family) {
    return this.bd.deleteFamily(family);
  }

  public boolean bornChild(Family family, String manName, String womanName) {
    try {
      if (this.bd.isFamilyNotExist(family)) return false;

      family.bornChild(manName, womanName);
      return true;

    } catch (Exception e) {
      System.out.println(e);
      return false;
    }
  }

  public Family adoptChild(Family family, Human human) {
    if (this.bd.isFamilyNotExist(family)) return null;

    family.addChild(human);
    return family;
  }

  public void deleteAllChildrenOlderThen(int age) {
    ArrayList<Human> childrenTmp = new ArrayList<Human>();
    this.bd.getAllFamilies()
        .forEach(f -> {
          f.getChildren().forEach(child -> {
            if ((2021 - child.getYear()) > age) {
              childrenTmp.add(child);
            }
          });
          childrenTmp.forEach(f::deleteChild);
          childrenTmp.clear();
        });
  }

  public int count() {
    return this.bd.getAllFamilies().size();
  }

  public Family getFamilyById(int id) {
    return this.bd.getFamilyByIndex(id);
  }

  public HashSet<Pet> getPets(int familyIndex) {
    if (this.bd.isFamilyNotExist(familyIndex)) return null;

    return this.getFamilyById(familyIndex).getPets();
  }

  public boolean addPet(int familyIndex, Pet pet) {
    if (this.bd.isFamilyNotExist(familyIndex)) return false;

    this.getFamilyById(familyIndex).addPet(pet);
    return true;
  }
}
