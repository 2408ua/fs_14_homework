package hw4;

public class App {
  public static void main(String[] args) {
    Pet pet01 = new Pet("dog", "Boy", 5, 49, new String[]{"run", "swimming", "sleep"});
    Human father01 = new Human("Bob", "Marley", 1950);
    Human mother01 = new Human("Mandy", "Marley", 1955);
    Human sun01 = new Human("Alex", "Marley", 1979, 95, new String[][]{{"Monday", "Swimming"}, {"Wednesday", "Painting"}, {"Friday", "Football"}});
    Human daughter01 = new Human("Barbara", "Marley", 1990, 95, new String[][]{{"Monday", "Dancing"}, {"Wednesday", "Painting"}, {"Friday", "Singing"}});

    Family family01 = new Family(mother01, father01);
    family01.addChild(sun01);
    family01.addChild(daughter01);
    family01.addPet(pet01);

    System.out.println("-----------------");
    System.out.println(family01.toString());
    System.out.println("Family count - " + family01.countFamily());
    System.out.println("-----------------");
    System.out.println(sun01.getFamily());
    boolean foo = family01.deleteChild(sun01);
    System.out.println(foo);
    System.out.println("-----------------");
    System.out.println("Sun family -  " + sun01.getFamily());
    System.out.println("-----------------");
    System.out.println(family01.toString());
    sun01.describePet();
    boolean foo1 = sun01.feedPet(false);
    System.out.print(foo1);
  }
}
