package hw4;

import java.util.Arrays;
import java.util.Objects;

public class Pet {
  private String species;
  private String nikname;
  private int age;
  private int trickLevel;
  private String[] habits;
  private Family family = null;

  static {
    System.out.println("Class Pet is loading.");
  }

  {
    System.out.println("Pet: new instance.");
  }

  Pet() {}

  Pet(String species, String nikname) {
    this(species, nikname, 5, 10, new String[]{});
  }

  Pet(String species, String nikname, int age, int trickLevel, String[] habits) {
    this.species = species;
    this.nikname = nikname;
    this.age = age;
    this.trickLevel = trickLevel;
    this.habits = habits;
  }

  public Family getFamily() {
    return family;
  }

  public void setFamily(Family family) {
    this.family = family;
  }

  public String getSpecies() {
    return species;
  }

  public String getNikname() {
    return nikname;
  }

  public int getAge() {
    return age;
  }

  public int getTrickLevel() {
    return trickLevel;
  }

  public String[] getHabits() {
    return habits;
  }

  public void eat() {
    System.out.print("Я кушаю!");
  }

  public void respond() {
    System.out.printf("Привет, хозяин. Я - %s. Я соскучился!", nikname);
  }

  public void foul() {
    System.out.print("Нужно хорошо замести следы...");
  }


  @Override
  public String toString() {
    return String.format("%s{nickname=%s, age=%d, trickLevel=%d, %s}", getSpecies(), getNikname(), getAge(), getTrickLevel(), Arrays.toString(getHabits()));
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Pet pet = (Pet) o;
    return age == pet.age &&
        trickLevel == pet.trickLevel &&
        Objects.equals(species, pet.species) &&
        Objects.equals(nikname, pet.nikname);
  }

  @Override
  public int hashCode() {

    int result = Objects.hash(species, nikname, age, trickLevel);
    result = 31 * result;
    return result;
  }
}
