package hw5;

import java.util.Arrays;
import java.util.Objects;

public class Family {
  private Pet pet;
  private final Human mother;
  private final Human father;
  private Human[] children;

  static {
    System.out.println("Class Family is loading.");
  }

  {
    System.out.println("Family: new instance.");
  }

  public Family(Human mother, Human father) {
    this.mother = mother;
    this.father = father;
    this.mother.setFamily(this);
    this.father.setFamily(this);
    this.children = new Human[0];
  }

  public Pet getPet() {
    return this.pet;
  }

  public Human getMother() {
    return this.mother;
  }

  public Human getFather() {
    return this.father;
  }

  public Human[] getChildren() {
    return this.children;
  }

  public void addChild(Human child) {
    int length = this.children.length + 1;
    Human[] tmp = new Human[length];
    System.arraycopy(this.children, 0, tmp, 0, this.children.length);
    tmp[length - 1] = child;
    this.children = tmp;
    this.children[length - 1].setFamily(this);
  }

  public void addPet(Pet pet) {
    this.pet = pet;
    pet.setFamily(this);
  }

  public void deletePet(Pet pet) {
    if (this.pet == pet) {
      pet.setFamily(null);
    }
  }

  public boolean deleteChild(int index) {
    int length = this.children.length;
    if (length == 0 || length - 1 < index) return false;
    this.children[index].setFamily(null);

    Human[] tmp = new Human[length - 1];
    for (int i = 0; i < length - 1; i++) {
      if (i < index) {
        tmp[i] = this.children[i];
        continue;
      }
      tmp[i] = this.children[i + 1];
    }
    this.children = tmp;
    return true;
  }

  public int countFamily() {
    return this.children.length + 2;
  }

  public int getCountChildren() {
    return this.children.length;
  }

  private boolean isNotChild(Human child) {
    for (Human item : this.children) {
      if (item.equals(child)) return false;
    }
    return true;
  }

  public boolean deleteChild(Human child) {
    if (children.length == 0) return false;
    if (this.isNotChild(child)) return false;

    child.setFamily(null);

    Human[] res = new Human[children.length - 1];
    int count = 0;
    for (Human item : this.children) {
      if (item.equals(child)) {
        continue;
      }
      res[count] = item;
      count++;
    }
    this.children = res;
    return true;
  }

  @Override
  public String toString() {
    return "Family{" +
        "pet=" + pet +
        ", mother=" + mother +
        ", father=" + father +
        ", children=" + Arrays.toString(children) +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Family family = (Family) o;
    return Objects.equals(this.mother, family.mother) &&
        Objects.equals(this.father, family.father) &&
        Arrays.equals(this.children, family.children);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(mother, father);
    result = 31 * result + Arrays.hashCode(children);
    return result;
  }

  @Override
  public void finalize() {
    System.out.println("Family has deleted");
  }
}
