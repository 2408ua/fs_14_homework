package hw5;

import java.sql.Array;

public class App {

  public static void main(String[] args) {

    int length = 1000000000;
    Human[] human = new Human[length];
    for (int i = 0; i < length; i++) {
      human[i] = new Human("Alex", "Marley", 1979, 95,
          new String[][]{{DayOfWeek.MONDAY.name(), ToDo.SWIMMING.name()},
              {DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name()},
              {DayOfWeek.FRIDAY.name(), ToDo.FOOTBALL.name()}});
    }
  }
}
