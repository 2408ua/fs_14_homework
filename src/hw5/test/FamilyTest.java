package hw5.test;

import hw5.*;
import org.junit.jupiter.api.Test;
import org.testng.Assert;

public class FamilyTest {
  Pet pet;
  Human father;
  Human mother;
  Human sun;
  Human daughter;
  Family family;

  public FamilyTest() {
    this.pet = new Pet(Species.DOG, "Boy", 5, 49, new String[]{"run", "swimming", "sleep"});
    this.father = new Human("Bob", "Marley", 1950);
    this.mother = new Human("Mandy", "Marley", 1955);
    this.sun = new Human("Alex", "Marley", 1979, 95,
        new String[][]{{DayOfWeek.MONDAY.name(), ToDo.SWIMMING.name()},
            {DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name()},
            {DayOfWeek.FRIDAY.name(), ToDo.FOOTBALL.name()}});
    this.daughter = new Human("Barbara", "Marley", 1990, 95, new String[][]{{DayOfWeek.MONDAY.name(), ToDo.DANCING.name()},
        {DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name()},
        {DayOfWeek.FRIDAY.name(), ToDo.SINGING.name()}});
    this.family = new Family(this.mother, this.father);
    this.family.addChild(this.sun);
    this.family.addChild(this.daughter);
    this.family.addPet(this.pet);
  }

  @Test
  public void countFamily(){
    Assert.assertEquals(this.family.countFamily(), 4);
  }
}
