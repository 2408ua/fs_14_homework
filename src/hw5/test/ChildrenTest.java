package hw5.test;

import hw5.*;
import org.junit.jupiter.api.Test;
import org.testng.Assert;

public class ChildrenTest {
  Pet pet;
  Human father;
  Human mother;
  Human sun;
  Human daughter;
  Family family;

  public ChildrenTest() {
    this.pet = new Pet(Species.DOG, "Boy", 5, 49, new String[]{"run", "swimming", "sleep"});
    this.father = new Human("Bob", "Marley", 1950);
    this.mother = new Human("Mandy", "Marley", 1955);
    this.sun = new Human("Alex", "Marley", 1979, 95,
        new String[][]{{DayOfWeek.MONDAY.name(), ToDo.SWIMMING.name()},
            {DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name()},
            {DayOfWeek.FRIDAY.name(), ToDo.FOOTBALL.name()}});
    this.daughter = new Human("Barbara", "Marley", 1990, 95, new String[][]{{DayOfWeek.MONDAY.name(), ToDo.DANCING.name()},
        {DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name()},
        {DayOfWeek.FRIDAY.name(), ToDo.SINGING.name()}});
    this.family = new Family(this.mother, this.father);
    this.family.addChild(this.sun);
    this.family.addChild(this.daughter);
    this.family.addPet(this.pet);
  }

  @Test
  public void should_delete_child_by_object() {
    Assert.assertTrue(this.family.deleteChild(this.sun));
    Assert.assertEquals(this.family.getCountChildren(), 1);
  }

  @Test
  public void should_not_delete_child_by_object() {
    Human sun = new Human("Dan", "Marley", 1979, 95,
        new String[][]{{DayOfWeek.MONDAY.name(), ToDo.SWIMMING.name()},
            {DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name()},
            {DayOfWeek.FRIDAY.name(), ToDo.FOOTBALL.name()}});
    Assert.assertFalse(this.family.deleteChild(sun));
    Assert.assertEquals(this.family.getCountChildren(), 2);
  }

  @Test
  public void should_delete_child_by_index() {
    Assert.assertTrue(this.family.deleteChild(1));
    Assert.assertEquals(this.family.getCountChildren(), 1);
  }

  @Test
  public void should_not_delete_child_by_index() {
    Assert.assertFalse(this.family.deleteChild(3));
    Assert.assertEquals(this.family.getCountChildren(), 2);
  }

  @Test
  public void should_add_child() {
    Human sun = new Human("Dan", "Marley", 1979, 95,
        new String[][]{{DayOfWeek.MONDAY.name(), ToDo.SWIMMING.name()},
            {DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name()},
            {DayOfWeek.FRIDAY.name(), ToDo.FOOTBALL.name()}});
    this.family.addChild(sun);
    Assert.assertEquals(this.family.getCountChildren(), 3);

    Human[] children = this.family.getChildren();
    Assert.assertEquals(children[children.length - 1], sun);

  }
}
