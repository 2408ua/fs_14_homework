package hw5.test;

import hw5.*;
import org.junit.jupiter.api.Test;
import org.testng.Assert;

public class ToStringTest {
  Pet pet;
  Human father;
  Human mother;
  Human sun;
  Human daughter;
  Family family;

  public ToStringTest() {
    this.pet = new Pet(Species.DOG, "Boy", 5, 49, new String[]{"run", "swimming", "sleep"});
    this.father = new Human("Bob", "Marley", 1950);
    this.mother = new Human("Mandy", "Marley", 1955);
    this.sun = new Human("Alex", "Marley", 1979, 95,
        new String[][]{{DayOfWeek.MONDAY.name(), ToDo.SWIMMING.name()},
            {DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name()},
            {DayOfWeek.FRIDAY.name(), ToDo.FOOTBALL.name()}});
    this.daughter = new Human("Barbara", "Marley", 1990, 95, new String[][]{{DayOfWeek.MONDAY.name(), ToDo.DANCING.name()},
        {DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name()},
        {DayOfWeek.FRIDAY.name(), ToDo.SINGING.name()}});
    this.family = new Family(this.mother, this.father);
    this.family.addChild(this.sun);
    this.family.addChild(this.daughter);
    this.family.addPet(this.pet);
  }

  @Test
  public void Pet_toString_test() {
    //given
    String petStr = "Species{canFly=false, numberOfLegs=4, hasFur=true}{nickname=Boy, age=5, trickLevel=49, [run, swimming, sleep]}";
    //then
    Assert.assertEquals(this.pet.toString(), petStr);
  }

  @Test
  public void Human_toString_test() {
    //given
    String humanStr = "Human{name=Alex, surname=Marley, year=1979, iq=95, schedule=[MONDAY, SWIMMING][WEDNESDAY, PAINTING][FRIDAY, FOOTBALL]}";
    //then
    Assert.assertEquals(this.sun.toString(), humanStr);
  }

  @Test
  public void Family_toString_test() {
    //given
    String familyStr = "Family{pet=Species{canFly=false, numberOfLegs=4, hasFur=true}{nickname=Boy, age=5, trickLevel=49," +
        " [run, swimming, sleep]}, mother=Human{name=Mandy, surname=Marley, year=1955, iq=90, schedule=[, ]}," +
        " father=Human{name=Bob, surname=Marley, year=1950, iq=90, schedule=[, ]}, children=Human{name=Alex," +
        " surname=Marley, year=1979, iq=95, schedule=[MONDAY, SWIMMING][WEDNESDAY, PAINTING][FRIDAY, FOOTBALL]}" +
        "Human{name=Barbara, surname=Marley, year=1990, iq=95, schedule=[MONDAY, DANCING][WEDNESDAY, PAINTING][FRIDAY, SINGING]}}";

    //then
    Assert.assertEquals(this.family.toString(), familyStr);
  }
}
