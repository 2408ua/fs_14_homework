package hw5.test;

import hw5.*;
import org.junit.jupiter.api.Test;
import org.testng.Assert;

public class EqualsHashCodeTest {
  Pet pet;
  Human father;
  Human mother;
  Human sun;
  Human daughter;
  Family family;

  public EqualsHashCodeTest() {
    this.pet = new Pet(Species.DOG, "Boy", 5, 49, new String[]{"run", "swimming", "sleep"});
    this.father = new Human("Bob", "Marley", 1950);
    this.mother = new Human("Mandy", "Marley", 1955);
    this.sun = new Human("Alex", "Marley", 1979, 95,
        new String[][]{{DayOfWeek.MONDAY.name(), ToDo.SWIMMING.name()},
            {DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name()},
            {DayOfWeek.FRIDAY.name(), ToDo.FOOTBALL.name()}});
    this.daughter = new Human("Barbara", "Marley", 1990, 95, new String[][]{{DayOfWeek.MONDAY.name(), ToDo.DANCING.name()},
        {DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name()},
        {DayOfWeek.FRIDAY.name(), ToDo.SINGING.name()}});
    this.family = new Family(this.mother, this.father);
    this.family.addChild(this.sun);
    this.family.addChild(this.daughter);
    this.family.addPet(this.pet);
  }

  @Test
  public void pet_true() {
    Pet pet = new Pet(Species.DOG, "Boy", 5, 49, new String[]{"run", "swimming", "sleep"});
    Assert.assertTrue(this.pet.equals(pet) && pet.equals(this.pet));
    Assert.assertEquals(pet.hashCode(), this.pet.hashCode());
  }

  @Test
  public void pet_false() {
    Pet pet = new Pet(Species.DOG, "Black", 5, 49, new String[]{"run", "swimming", "sleep"});
    Assert.assertFalse(this.pet.equals(pet) || pet.equals(this.pet));
    Assert.assertNotEquals(pet.hashCode(), this.pet.hashCode());
  }

  @Test
  public void human_true() {
    Human human = new Human("Alex", "Marley", 1979, 95,
        new String[][]{{DayOfWeek.MONDAY.name(), ToDo.SWIMMING.name()},
            {DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name()},
            {DayOfWeek.FRIDAY.name(), ToDo.FOOTBALL.name()}});

    Assert.assertTrue(this.sun.equals(human) && human.equals(this.sun));
    Assert.assertEquals(this.sun.hashCode(), human.hashCode());
  }

  @Test
  public void human_false() {
    Human human = new Human("Jon", "Marley", 1979, 95,
        new String[][]{{DayOfWeek.MONDAY.name(), ToDo.SWIMMING.name()},
            {DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name()},
            {DayOfWeek.FRIDAY.name(), ToDo.FOOTBALL.name()}});

    Assert.assertFalse(this.sun.equals(human) || human.equals(this.sun));
    Assert.assertNotEquals(this.sun.hashCode(), human.hashCode());
  }

  @Test
  public void family_true() {
    Human father = new Human("Bob", "Marley", 1950);
    Human mother = new Human("Mandy", "Marley", 1955);
    Human sun = new Human("Alex", "Marley", 1979, 95,
        new String[][]{{DayOfWeek.MONDAY.name(), ToDo.SWIMMING.name()},
            {DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name()},
            {DayOfWeek.FRIDAY.name(), ToDo.FOOTBALL.name()}});
    Human daughter = new Human("Barbara", "Marley", 1990,
        95, new String[][]{{DayOfWeek.MONDAY.name(), ToDo.DANCING.name()},
        {DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name()},
        {DayOfWeek.FRIDAY.name(), ToDo.SINGING.name()}});
    Family family = new Family(mother, father);
    family.addChild(sun);
    family.addChild(daughter);

    Assert.assertTrue(this.family.equals(family) && family.equals(this.family));
    Assert.assertEquals(this.family.hashCode(), family.hashCode());
  }

  @Test
  public void family_false() {
    Human father = new Human("Jon", "Marley", 1950);
    Human mother = new Human("Mandy", "Marley", 1955);
    Human sun = new Human("Alex", "Marley", 1979, 95,
        new String[][]{{DayOfWeek.MONDAY.name(), ToDo.SWIMMING.name()},
            {DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name()},
            {DayOfWeek.FRIDAY.name(), ToDo.FOOTBALL.name()}});
    Human daughter = new Human("Barbara", "Marley", 1990,
        95, new String[][]{{DayOfWeek.MONDAY.name(), ToDo.DANCING.name()},
        {DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name()},
        {DayOfWeek.FRIDAY.name(), ToDo.SINGING.name()}});
    Family family = new Family(mother, father);
    family.addChild(sun);
    family.addChild(daughter);

    Assert.assertFalse(this.family.equals(family) || family.equals(this.family));
    Assert.assertNotEquals(this.family.hashCode(), family.hashCode());
  }
}
