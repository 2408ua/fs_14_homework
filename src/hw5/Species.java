package hw5;

public enum Species {
  CAT(false, 4, true),
  DOG(false, 4, true),
  PIG(false, 4, true),
  PARROT(true, 2, false),
  HAMSTER(false, 4, true);
  private final boolean canFly;
  private final int numberOfLegs;
  private final boolean hasFur;

  Species(boolean canFly, int numberOfLegs, boolean hasFur) {
    this.canFly = canFly;
    this.numberOfLegs = numberOfLegs;
    this.hasFur = hasFur;
  }

  public boolean isCanFly() {
    return canFly;
  }

  public int getNumberOfLegs() {
    return numberOfLegs;
  }

  public boolean isHasFur() {
    return hasFur;
  }

  @Override
  public String toString() {
    return "Species{" +
        "canFly=" + canFly +
        ", numberOfLegs=" + numberOfLegs +
        ", hasFur=" + hasFur +
        '}';
  }
}
