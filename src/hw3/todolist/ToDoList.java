package hw3.todolist;

import java.util.Arrays;
import java.util.Scanner;

public class ToDoList {
  private final String[][] schedule;
  private final Scanner scan;

  ToDoList(String[][] sdl) {
    schedule = new String[7][2];
    for (int i = 0, ln = sdl.length; i < ln; i++) {
      System.arraycopy(sdl[i], 0, schedule[i], 0, sdl[i].length);
    }
    scan = new Scanner(System.in);
  }

  private void printf(String day, String str) {
    System.out.printf("Your tasks for %s: %s\n", day, str);
  }

  private String inputData(String str) {
    System.out.print(str);
    return scan.nextLine();
  }

  private String getTargetByDay(String day) {
    String res = "empty";
    for (String[] strings : schedule) {
      String tmp = strings[0].toLowerCase();
      if (tmp.equals(day)) {
        res = strings[1];
      }
    }
    return res;
  }

  private void sorry() {
    System.out.print("Sorry, I don't understand you, please try again\n");
  }

  private void changeTarget(String day, String target) {
    for (int i = 0, ln = schedule.length; i < ln; i++) {
      String tmp = schedule[i][0].toLowerCase();
      if (tmp.equals(day)) {
        schedule[i][1] = target;
      }
    }
  }

  public void run() {
    boolean loop = true;

    while (loop) {
      final String day = inputData("Please, input the day of the week: ");
      String[] commandArr = day.toLowerCase().split(" ");
      System.out.println(Arrays.toString(commandArr));
      switch (commandArr[0]) {
        case "monday":
        case "tuesday":
        case "wednesday":
        case "thursday":
        case "friday":
        case "saturday":
        case "sunday":
          printf(commandArr[0], getTargetByDay(commandArr[0]));
          break;
        case "change":
        case "reschedule":
          if (commandArr.length < 2 || getTargetByDay(commandArr[1]).equals("empty")) {
            sorry();
            break;
          }
          final String newTarget = inputData("Input new target: ");
          changeTarget(commandArr[1], newTarget);
          break;
        case "exit":
          loop = false;
          break;
        default:
          sorry();
      }
    }
  }
}

