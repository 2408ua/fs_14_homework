package hw3.todolist;

public class App {
  public static void main(String[] args) {
    final String[][] schedule = {
        {"Monday", "go to work"},
        {"Tuesday", "go to work"},
        {"Wednesday", "go to work"},
        {"Thursday", "go to work"},
        {"Friday", "go to work"},
        {"Saturday", "learn to fly"},
        {"Sunday", "fly away"}
    };
    ToDoList toDo = new ToDoList(schedule);
    toDo.run();
  }
}
