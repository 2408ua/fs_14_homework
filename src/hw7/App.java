package hw7;

import hw6.human.DayOfWeek;
import hw6.human.ToDo;
import hw7.human.Human;
import hw7.human.Man;
import hw7.human.Woman;
import hw7.pet.Dog;

import java.util.HashMap;
import java.util.HashSet;


public class App {


  public static void main(String[] args) {
    HashSet<String> habits = new HashSet<String>();
    habits.add("run");
    habits.add("swimming");
    habits.add("sleep");
    Dog pet01 = new Dog("Boy", 5, 49, habits);
    Man father01 = new Man("Bob", "Marley", 1950, 90);
    Woman mother01 = new Woman("Mandy", "Marley", 1955, 100);

    HashMap<String, String> sunSchedule = new HashMap<>();
    sunSchedule.put(DayOfWeek.MONDAY.name(), ToDo.SWIMMING.name());
    sunSchedule.put(DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name());
    sunSchedule.put(DayOfWeek.FRIDAY.name(), ToDo.FOOTBALL.name());

    Man sun01 = new Man("Alex", "Marley", 1979, 95, sunSchedule);

    HashMap<String, String> daughterSchedule = new HashMap<>();
    sunSchedule.put(DayOfWeek.MONDAY.name(), ToDo.DANCING.name());
    sunSchedule.put(DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name());
    sunSchedule.put(DayOfWeek.FRIDAY.name(), ToDo.SINGING.name());

    Woman daughter01 = new Woman("Barbara", "Marley", 1990, 95, daughterSchedule);

    Family family01 = new Family(mother01, father01);
    family01.addChild(sun01);
    family01.addChild(daughter01);
    family01.addPet(pet01);

    System.out.println("-----------------");
    System.out.println(family01.toString());
    System.out.println("Family count - " + family01.countFamily());
    System.out.println("-----------------");

    Human newChild = family01.bornChild();
    System.out.println(newChild.toString());

    System.out.println("-----------------");
    System.out.println(family01.toString());
    System.out.println("Family count - " + family01.countFamily());
    System.out.println("-----------------");

    father01.repairCar();
    mother01.makeUp();
    pet01.respond();
    System.out.println('\n');
    System.out.println(pet01.toString());
  }
}
