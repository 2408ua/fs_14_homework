package hw7.test;

import hw7.Family;
import hw7.human.*;
import hw7.pet.*;
import org.junit.jupiter.api.Test;
import org.testng.Assert;

import java.util.HashMap;
import java.util.HashSet;

public class FamilyTest {
  Pet pet;
  Man father;
  Woman mother;
  Human sun;
  Human daughter;
  Family family;

  public FamilyTest() {
    HashSet<String> habits = new HashSet<String>();
    habits.add("run");
    habits.add("swimming");
    habits.add("sleep");
    this.pet = new Dog("Boy", 5, 49, habits);
    this.father = new Man("Bob", "Marley", 1950, 90);
    this.mother = new Woman("Mandy", "Marley", 1955, 100);
    HashMap<String, String> sunSchedule = new HashMap<>();
    sunSchedule.put(hw6.human.DayOfWeek.MONDAY.name(), hw6.human.ToDo.SWIMMING.name());
    sunSchedule.put(hw6.human.DayOfWeek.WEDNESDAY.name(), hw6.human.ToDo.PAINTING.name());
    sunSchedule.put(hw6.human.DayOfWeek.FRIDAY.name(), hw6.human.ToDo.FOOTBALL.name());

    this.sun = new Man("Alex", "Marley", 1979, 95, sunSchedule);

    HashMap<String, String> daughterSchedule = new HashMap<>();
    sunSchedule.put(hw6.human.DayOfWeek.MONDAY.name(), hw6.human.ToDo.DANCING.name());
    sunSchedule.put(hw6.human.DayOfWeek.WEDNESDAY.name(), hw6.human.ToDo.PAINTING.name());
    sunSchedule.put(hw6.human.DayOfWeek.FRIDAY.name(), hw6.human.ToDo.SINGING.name());

    this.daughter = new Woman("Barbara", "Marley", 1990, 95, daughterSchedule);
    this.family = new Family(this.mother, this.father);
    this.family.addChild(this.sun);
    this.family.addChild(this.daughter);
    this.family.addPet(this.pet);
  }

  @Test
  public void countFamily(){
    Assert.assertEquals(this.family.countFamily(), 4);
  }
}
