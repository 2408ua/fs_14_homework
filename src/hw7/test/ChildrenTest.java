package hw7.test;

import hw7.Family;
import hw7.human.*;
import hw7.pet.*;
import org.junit.jupiter.api.Test;
import org.testng.Assert;

import java.util.*;

public class ChildrenTest {
  Pet pet;
  Man father;
  Woman mother;
  Human sun;
  Human daughter;
  Family family;

  public ChildrenTest() {
    HashSet<String> habits = new HashSet<String>();
    habits.add("run");
    habits.add("swimming");
    habits.add("sleep");
    this.pet = new Dog("Boy", 5, 49, habits);
    this.father = new Man("Bob", "Marley", 1950, 90);
    this.mother = new Woman("Mandy", "Marley", 1955, 100);
    HashMap<String, String> sunSchedule = new HashMap<>();
    sunSchedule.put(hw6.human.DayOfWeek.MONDAY.name(), hw6.human.ToDo.SWIMMING.name());
    sunSchedule.put(hw6.human.DayOfWeek.WEDNESDAY.name(), hw6.human.ToDo.PAINTING.name());
    sunSchedule.put(hw6.human.DayOfWeek.FRIDAY.name(), hw6.human.ToDo.FOOTBALL.name());

    this.sun = new Man("Alex", "Marley", 1979, 95, sunSchedule);

    HashMap<String, String> daughterSchedule = new HashMap<>();
    sunSchedule.put(hw6.human.DayOfWeek.MONDAY.name(), hw6.human.ToDo.DANCING.name());
    sunSchedule.put(hw6.human.DayOfWeek.WEDNESDAY.name(), hw6.human.ToDo.PAINTING.name());
    sunSchedule.put(hw6.human.DayOfWeek.FRIDAY.name(), hw6.human.ToDo.SINGING.name());

    this.daughter = new Woman("Barbara", "Marley", 1990, 95, daughterSchedule);
    this.family = new Family(this.mother, this.father);
    this.family.addChild(this.sun);
    this.family.addChild(this.daughter);
    this.family.addPet(this.pet);
  }

  @Test
  public void should_delete_child_by_object() {
    Assert.assertTrue(this.family.deleteChild(this.sun));
    Assert.assertEquals(this.family.getCountChildren(), 1);
  }

  @Test
  public void should_not_delete_child_by_object() {
    HashMap<String, String> sunSchedule = new HashMap<>();
    sunSchedule.put(hw6.human.DayOfWeek.MONDAY.name(), hw6.human.ToDo.SWIMMING.name());
    sunSchedule.put(hw6.human.DayOfWeek.WEDNESDAY.name(), hw6.human.ToDo.PAINTING.name());
    sunSchedule.put(hw6.human.DayOfWeek.FRIDAY.name(), hw6.human.ToDo.FOOTBALL.name());

    Human sun = new Man("Dan", "Marley", 1979, 95, sunSchedule);
    Assert.assertFalse(this.family.deleteChild(sun));
    Assert.assertEquals(this.family.getCountChildren(), 2);
  }

  @Test
  public void should_delete_child_by_index() {
    Assert.assertTrue(this.family.deleteChild(1));
    Assert.assertEquals(this.family.getCountChildren(), 1);
  }

  @Test
  public void should_not_delete_child_by_index() {
    Assert.assertFalse(this.family.deleteChild(3));
    Assert.assertEquals(this.family.getCountChildren(), 2);
  }

  @Test
  public void should_add_child() {
    HashMap<String, String> sunSchedule = new HashMap<>();
    sunSchedule.put(hw6.human.DayOfWeek.MONDAY.name(), hw6.human.ToDo.SWIMMING.name());
    sunSchedule.put(hw6.human.DayOfWeek.WEDNESDAY.name(), hw6.human.ToDo.PAINTING.name());
    sunSchedule.put(hw6.human.DayOfWeek.FRIDAY.name(), hw6.human.ToDo.FOOTBALL.name());

    Human sun = new Man("Dan", "Marley", 1979, 95, sunSchedule);
    this.family.addChild(sun);
    Assert.assertEquals(this.family.getCountChildren(), 3);

    List<Human> children = this.family.getChildren();
    Assert.assertEquals(children.get(children.size() - 1), sun);

  }
}
