package hw7.test;

import hw7.Family;
import hw7.human.*;
import hw7.pet.*;
import org.junit.jupiter.api.Test;
import org.testng.Assert;

import java.util.HashMap;
import java.util.HashSet;

public class ToStringTest {
  Pet pet;
  Man father;
  Woman mother;
  Human sun;
  Human daughter;
  Family family;

  public ToStringTest() {
    HashSet<String> habits = new HashSet<String>();
    habits.add("run");
    habits.add("swimming");
    habits.add("sleep");
    this.pet = new Dog("Boy", 5, 49, habits);
    this.father = new Man("Bob", "Marley", 1950, 90);
    this.mother = new Woman("Mandy", "Marley", 1955, 100);
    HashMap<String, String> sunSchedule = new HashMap<>();
    sunSchedule.put(hw6.human.DayOfWeek.MONDAY.name(), hw6.human.ToDo.SWIMMING.name());
    sunSchedule.put(hw6.human.DayOfWeek.WEDNESDAY.name(), hw6.human.ToDo.PAINTING.name());
    sunSchedule.put(hw6.human.DayOfWeek.FRIDAY.name(), hw6.human.ToDo.FOOTBALL.name());

    this.sun = new Man("Alex", "Marley", 1979, 95, sunSchedule);

    HashMap<String, String> daughterSchedule = new HashMap<>();
    sunSchedule.put(hw6.human.DayOfWeek.MONDAY.name(), hw6.human.ToDo.DANCING.name());
    sunSchedule.put(hw6.human.DayOfWeek.WEDNESDAY.name(), hw6.human.ToDo.PAINTING.name());
    sunSchedule.put(hw6.human.DayOfWeek.FRIDAY.name(), hw6.human.ToDo.SINGING.name());

    this.daughter = new Woman("Barbara", "Marley", 1990, 95, daughterSchedule);
    this.family = new hw7.Family(this.mother, this.father);
    this.family.addChild(this.sun);
    this.family.addChild(this.daughter);
    this.family.addPet(this.pet);
  }

  @Test
  public void Pet_toString_test() {
    //given
    String petStr = "Pet{species=Species{canFly=false, numberOfLegs=0, hasFur=false}, nikname='Boy', age=5, trickLevel=49, habits=[sleep, swimming, run], family=true}";
    //then
    Assert.assertEquals(this.pet.toString(), petStr);
  }

  @Test
  public void Human_toString_test() {
    //given
    String humanStr = "Human{name=Alex, surname=Marley, year=1979, iq=95, schedule={WEDNESDAY=PAINTING, MONDAY=DANCING, FRIDAY=SINGING}}";
    //then
    Assert.assertEquals(this.sun.toString(), humanStr);
  }

  @Test
  public void Family_toString_test() {
    //given
    String familyStr = "Family{pet=[Pet{species=Species{canFly=false, numberOfLegs=0, hasFur=false}, nikname='Boy', age=5," +
        " trickLevel=49, habits=[sleep, swimming, run], family=true}], mother=Human{name=Mandy, surname=Marley, year=1955," +
        " iq=100, schedule={}}, father=Human{name=Bob, surname=Marley, year=1950, iq=90, schedule={}}, children=[Human{name=Alex," +
        " surname=Marley, year=1979, iq=95, schedule={WEDNESDAY=PAINTING, MONDAY=DANCING, FRIDAY=SINGING}}, Human{name=Barbara," +
        " surname=Marley, year=1990, iq=95, schedule={}}]}";

    //then
    Assert.assertEquals(this.family.toString(), familyStr);
  }
}
