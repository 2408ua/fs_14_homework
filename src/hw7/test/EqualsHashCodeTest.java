package hw7.test;

import hw7.Family;
import hw7.human.*;
import hw7.pet.*;
import org.junit.jupiter.api.Test;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class EqualsHashCodeTest {
  Pet pet;
  Man father;
  Woman mother;
  Human sun;
  Human daughter;
  Family family;

  public EqualsHashCodeTest() {
    HashSet<String> habits = new HashSet<String>();
    habits.add("run");
    habits.add("swimming");
    habits.add("sleep");
    this.pet = new Dog("Boy", 5, 49, habits);
    this.father = new Man("Bob", "Marley", 1950, 90);
    this.mother = new Woman("Mandy", "Marley", 1955, 100);
    HashMap<String, String> sunSchedule = new HashMap<>();
    sunSchedule.put(DayOfWeek.MONDAY.name(), ToDo.SWIMMING.name());
    sunSchedule.put(DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name());
    sunSchedule.put(DayOfWeek.FRIDAY.name(), ToDo.FOOTBALL.name());

    this.sun = new Man("Alex", "Marley", 1979, 95, sunSchedule);

    HashMap<String, String> daughterSchedule = new HashMap<>();
    daughterSchedule.put(DayOfWeek.MONDAY.name(), ToDo.DANCING.name());
    daughterSchedule.put(DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name());
    daughterSchedule.put(DayOfWeek.FRIDAY.name(), ToDo.SINGING.name());

    this.daughter = new Woman("Barbara", "Marley", 1990, 95, daughterSchedule);
    this.family = new Family(this.mother, this.father);
    this.family.addChild(this.sun);
    this.family.addChild(this.daughter);
    this.family.addPet(this.pet);
  }

  @Test
  public void pet_true() {
    HashSet<String> habits = new HashSet<String>();
    habits.add("run");
    habits.add("swimming");
    habits.add("sleep");
    Pet pet = new Dog("Boy", 5, 49, habits);
    Assert.assertTrue(this.pet.equals(pet) && pet.equals(this.pet));
    Assert.assertEquals(pet.hashCode(), this.pet.hashCode());
  }

  @Test
  public void pet_false() {
    HashSet<String> habits = new HashSet<String>();
    habits.add("run");
    habits.add("swimming");
    habits.add("sleep");
    Pet pet = new Dog("Black", 5, 49, habits);
    Assert.assertFalse(this.pet.equals(pet) || pet.equals(this.pet));
    Assert.assertNotEquals(pet.hashCode(), this.pet.hashCode());
  }

  @Test
  public void human_true() {
    HashMap<String, String> sunSchedule = new HashMap<>();
    sunSchedule.put(DayOfWeek.MONDAY.name(), ToDo.SWIMMING.name());
    sunSchedule.put(DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name());
    sunSchedule.put(DayOfWeek.FRIDAY.name(), ToDo.FOOTBALL.name());
    Man sun = new Man("Alex", "Marley", 1979, 95, sunSchedule);
    Assert.assertTrue(this.sun.equals(sun) && sun.equals(this.sun));
    Assert.assertEquals(this.sun.hashCode(), sun.hashCode());
  }

  @Test
  public void human_false() {
    HashMap<String, String> sunSchedule = new HashMap<>();
    sunSchedule.put(DayOfWeek.MONDAY.name(), ToDo.SWIMMING.name());
    sunSchedule.put(DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name());
    sunSchedule.put(DayOfWeek.FRIDAY.name(), ToDo.FOOTBALL.name());
    Human human = new Man("Jon", "Marley", 1979, 95, sunSchedule);
    Assert.assertFalse(this.sun.equals(human) || human.equals(this.sun));
    Assert.assertNotEquals(this.sun.hashCode(), human.hashCode());
  }

  @Test
  public void family_true() {
    Man father = new Man("Bob", "Marley", 1950, 90);
    Woman mother = new Woman("Mandy", "Marley", 1955, 100);
    HashMap<String, String> sunSchedule = new HashMap<>();
    sunSchedule.put(DayOfWeek.MONDAY.name(), ToDo.SWIMMING.name());
    sunSchedule.put(DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name());
    sunSchedule.put(DayOfWeek.FRIDAY.name(), ToDo.FOOTBALL.name());
    Human sun = new Man("Alex", "Marley", 1979, 95, sunSchedule);

    HashMap<String, String> daughterSchedule = new HashMap<>();
    daughterSchedule.put(DayOfWeek.MONDAY.name(), ToDo.DANCING.name());
    daughterSchedule.put(DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name());
    daughterSchedule.put(DayOfWeek.FRIDAY.name(), ToDo.SINGING.name());
    Human daughter = new Woman("Barbara", "Marley", 1990, 95, daughterSchedule);
    
    Family family = new Family(mother, father);
    family.addChild(sun);
    family.addChild(daughter);

    Assert.assertTrue(this.family.equals(family) && family.equals(this.family));
    Assert.assertEquals(this.family.hashCode(), family.hashCode());
  }

  @Test
  public void family_false() {
    Man father = new Man("Jon", "Marley", 1950, 90);
    Woman mother = new Woman("Mandy", "Marley", 1955, 100);
    HashMap<String, String> sunSchedule = new HashMap<>();
    sunSchedule.put(DayOfWeek.MONDAY.name(), ToDo.SWIMMING.name());
    sunSchedule.put(DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name());
    sunSchedule.put(DayOfWeek.FRIDAY.name(), ToDo.FOOTBALL.name());
    Human sun = new Man("Alex", "Marley", 1979, 95, sunSchedule);

    HashMap<String, String> daughterSchedule = new HashMap<>();
    daughterSchedule.put(DayOfWeek.MONDAY.name(), ToDo.DANCING.name());
    daughterSchedule.put(DayOfWeek.WEDNESDAY.name(), ToDo.PAINTING.name());
    daughterSchedule.put(DayOfWeek.FRIDAY.name(), ToDo.SINGING.name());
    Human daughter = new Woman("Barbara", "Marley", 1990, 95, daughterSchedule);

    Family family = new Family(mother, father);
    family.addChild(sun);
    family.addChild(daughter);

    Assert.assertFalse(this.family.equals(family) || family.equals(this.family));
    Assert.assertNotEquals(this.family.hashCode(), family.hashCode());
  }
}
