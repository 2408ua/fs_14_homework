package hw7;

import hw7.human.*;
import hw7.pet.Pet;

import java.util.*;

public class Family implements HumanCreator {
  private final HashSet<Pet> pets;
  private final Woman mother;
  private final Man father;
  private final ArrayList<Human> children;


  static {
    System.out.println("Class Family is loading.");
  }

  {
    System.out.println("Family: new instance.");
  }

  public Family(Woman mother, Man father) {
    this.mother = mother;
    this.father = father;
    this.mother.setFamily(this);
    this.father.setFamily(this);
    this.children = new ArrayList<Human>();
    this.pets = new HashSet<Pet>();
  }

  public Family(Human mother, Human father) {
    this.mother = (Woman) mother;
    this.father = (Man) father;
    this.mother.setFamily(this);
    this.father.setFamily(this);
    this.children = new ArrayList<Human>();
    this.pets = new HashSet<Pet>();
  }

  public HashSet<Pet> getPets() {
    return this.pets;
  }

  public Woman getMother() {
    return this.mother;
  }

  public Man getFather() {
    return this.father;
  }

  public ArrayList<Human> getChildren() {
    return this.children;
  }

  public void addChild(Human child) {
    child.setFamily(this);
    this.children.add(child);
  }

  public void addPet(Pet pet) {
    this.pets.add((Pet) pet);
    pet.setFamily(this);
  }

  public void deletePet(Pet pet) {
    if (this.pets.remove(pet)) {
      pet.setFamily(null);
    }
  }

  public boolean deleteChild(int index) {
    int length = this.children.size();
    if (length == 0 || length - 1 < index) return false;
    this.children.get(index).setFamily(null);
    this.children.remove(index);
    return true;
  }

  public int countFamily() {
    return this.children.size() + 2;
  }

  public int getCountChildren() {
    return this.children.size();
  }

  private boolean isNotChild(Human child) {
    for (Human item : this.children) {
      if (item.equals(child)) return false;
    }
    return true;
  }

  public boolean deleteChild(Human child) {
    if (children.size() == 0) return false;
    if (this.isNotChild(child)) return false;

    child.setFamily(null);
    children.remove(child);
    return true;
  }

  @Override
  public String toString() {
    return "Family{" +
        "pet=" + pets.toString() +
        ", mother=" + mother +
        ", father=" + father +
        ", children=" + children.toString() +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Family family = (Family) o;
    return Objects.equals(this.mother, family.mother) &&
        Objects.equals(this.father, family.father) &&
        Objects.equals(this.children, family.children);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(mother, father);
    result = 31 * result + Objects.hashCode(children);
    return result;
  }

  @Override
  public void finalize() {
    System.out.println("Family has deleted");
  }

  @Override
  public Human bornChild() {
    boolean random = Math.random() < 0.5;
    String fatherSurname = this.father.getSurname();
    String name = random ? Names.WOMAN.getRandomName() : Names.MAN.getRandomName();
    int iq = (this.father.getIQ() + this.mother.getIQ()) / 2;
    Human newHuman = random ? new Woman(name, fatherSurname, 2021, iq, this) : new Man(name, fatherSurname, 2021, iq, this);
    addChild(newHuman);
    return newHuman;
  }

  @Override
  public Human bornChild(String manName, String womanName) {
    boolean random = Math.random() < 0.5;
    String fatherSurname = this.father.getSurname();
    String name = random ? womanName : manName;
    int iq = (this.father.getIQ() + this.mother.getIQ()) / 2;
    Human newHuman = random ? new Woman(name, fatherSurname, 2021, iq, this) : new Man(name, fatherSurname, 2021, iq, this);
    addChild(newHuman);
    return newHuman;
  }
}
