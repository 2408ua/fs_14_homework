package hw7.human;

import hw7.Family;

import java.util.HashMap;

public final class Man extends Human {
  public Man(String name, String surname, int year, int IQ) {
    super(name, surname, year, IQ);
  }

  public Man(String name, String surname, int year, int IQ, HashMap<String, String> schedule) {
    super(name, surname, year, IQ, schedule);
  }

  public Man(String name, String surname, int year, int iq, Family family) {
    super(name, surname, year, iq, family);
  }

  public Man(String name, String surname, int year) {
    super(name, surname, year);
  }

  public Man() {
  }

  public void repairCar() {
    System.out.println("Я чиню машину, меня не беспокоить!");
  }

  public void greetPet() {
    System.out.println("Привет, зверьё моё!");
  }
}
