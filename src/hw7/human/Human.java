package hw7.human;

import hw7.Family;
import hw7.pet.Pet;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;

public class Human {
  private String name;
  private String surname;
  private int year;
  private int IQ;
  private HashMap<String, String> schedule;
  private Family family = null;

  static {
    System.out.println("Class Human is loading.");
  }

  {
    System.out.println("Human: new instance.");
  }

  Human() {
  }

  public Human(String name, String surname, int year, int iq, Family family) {
    this(name, surname, year, iq, new HashMap<String, String>(), family);
  }

  public Human(String name, String surname, int year) {
    this(name, surname, year, 90, new HashMap<String, String>());
  }

  public Human(String name, String surname, int year, int iq) {
    this(name, surname, year, iq, new HashMap<String, String>());
  }

  public Human(String name, String surname, int year, int IQ, HashMap<String, String> schedule) {
    this.name = name;
    this.surname = surname;
    this.year = year;
    this.IQ = IQ;
    this.schedule = schedule;
  }

  public Human(String name, String surname, int year, int IQ, HashMap<String, String> schedule, Family family) {
    this.name = name;
    this.surname = surname;
    this.year = year;
    this.IQ = IQ;
    this.schedule = schedule;
    this.family = family;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public int getYear() {
    return year;
  }

  public int getIQ() {
    return IQ;
  }

  public Family getFamily() {
    return this.family;
  }

  public void setFamily(Family family) {
    this.family = family;
  }

  public boolean feedPet(boolean isFeedTime, String nikname) {
    if (family != null) {
      HashSet<Pet> pets = family.getPets();
      for (Pet p : pets) {
        if (p.getNikname().equals(nikname)) {
          if (isFeedTime || p.getTrickLevel() > Math.random() * 100) {
            System.out.printf("Хм... покормлю ка я %s.\n", p.getNikname());
            return true;
          }
          System.out.printf("Думаю, %s не голоден.\n", p.getNikname());
          return false;
        }
      }
    }
    return false;
  }

  public void describePets() {
    if (family != null) {
      HashSet<Pet> pets = family.getPets();
      for (Pet p : pets) {
        System.out.printf("У меня есть %s, ему %d лет, он %s\n", p.getSpecies(), p.getAge(), p.getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый");
      }
    }
    System.out.println("No family, no pet!");
  }

  public String toString() {
    return String.format("Human{name=%s, surname=%s, year=%d, iq=%d, schedule=%s, family=%s}", name, surname, year, IQ, schedule.toString(), family != null);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Human human = (Human) o;
    return year == human.year &&
        IQ == human.IQ
        && Objects.equals(name, human.name)
        && Objects.equals(surname, human.surname)
        && schedule.equals(human.schedule);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(name, surname, year, IQ);
    return 31 * result + Objects.hashCode(schedule);
  }

  @Override
  public void finalize() {
    System.out.println("Human has deleted");
  }
}
