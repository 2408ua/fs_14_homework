package hw7.pet;

import java.util.*;
import hw7.Family;

public abstract class Pet {
  private final Species species;
  private final String nikname;
  private final int age;
  private final int trickLevel;
  private final HashSet<String> habits;
  private Family family = null;

  public Pet(Species species, String nikname, int age, int trickLevel, HashSet<String> habits) {
    this.species = species;
    this.nikname = nikname;
    this.age = age;
    this.trickLevel = trickLevel;
    this.habits = habits;
  }

  public Pet(Species species, String nikname, int age, int trickLevel) {
    this.species = species;
    this.nikname = nikname;
    this.age = age;
    this.trickLevel = trickLevel;
    this.habits = new HashSet<String>();
  }

  public Pet(String nikname, int age, int trickLevel) {
    this.species = Species.UNKNOWN;
    this.nikname = nikname;
    this.age = age;
    this.trickLevel = trickLevel;
    this.habits = new HashSet<String>();
  }

  public Species getSpecies() {
    return species;
  }

  public Family getFamily() {
    return family;
  }

  public void setFamily(Family family) {
    this.family = family;
  }

  public String getNikname() {
    return nikname;
  }

  public int getAge() {
    return age;
  }

  public int getTrickLevel() {
    return trickLevel;
  }

  public HashSet<String> getHabits() {
    return habits;
  }

  @Override
  public String toString() {
    String familyStr = family != null ? ", family=" + true : "";
    return "Pet{" +
        "species=" + species +
        ", nikname='" + nikname + '\'' +
        ", age=" + age +
        ", trickLevel=" + trickLevel +
        ", habits=" + habits +
        familyStr +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Pet pet = (Pet) o;
    return age == pet.age && trickLevel == pet.trickLevel && species == pet.species && nikname.equals(pet.nikname);
  }

  @Override
  public int hashCode() {
    return Objects.hash(species, nikname, age, trickLevel);
  }

  public void eat() {
    System.out.print("Я кушаю!");
  }

  public abstract void respond();
}
