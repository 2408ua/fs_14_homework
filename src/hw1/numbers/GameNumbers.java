package hw1.numbers;

import javax.print.attribute.standard.RequestingUserName;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;

public class GameNumbers {
  private final int randomNumber;
  private int[] numbers;

  public GameNumbers(int from, int to) {
    Random random = new Random();
    this.randomNumber = random.nextInt(to) + from + 1;
    numbers = new int[0];
  }

  private void setNumbers(int num) {
    int ln = numbers.length + 1;
    int[] ints = new int[ln];
    ints[ln - 1] = num;
    numbers = ints;
  }

  private int[] getNumbers() {
    return numbers;
  }

  private void println(String str) {
    System.out.println(str);
  }

  private void print(String str) {
    System.out.print(str);
  }

  private boolean isNotNumeric(String str) {
    try {
      Integer.parseInt(str);
      return false;
    } catch (Exception e) {
      return true;
    }
  }

  public void game() {
    String answer;
    String name;
    boolean game = true;
    println("Let the game begin!");
    Scanner scanName = new Scanner(System.in);
    print("Enter name: ");
    name = scanName.nextLine();
    Scanner scanNumber = new Scanner(System.in);


    while (game) {
      print("Enter number: ");
      answer = scanNumber.nextLine();

      if (isNotNumeric(answer)) {
        println("Input correct number!");
        continue;
      }
      int number = Integer.parseInt(answer);

      if (number < this.randomNumber) {
        println("Your number is too small. Please, try again.");
      } else if (number > this.randomNumber) {
        println("Your number is too big. Please, try again.");
      } else {
        System.out.printf("Congratulations, %s!", name);
        System.out.printf("Your numbers: %s!", Arrays.toString(getNumbers()));
        game = false;
      }
      setNumbers(number);
    }
  }
}
