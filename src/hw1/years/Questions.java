package hw1.years;

import java.util.Random;

public class Questions {
  private final String[][] questions = {
      {"When did the World War II begin?\n", "1939"},
      {"When did Nobel event dynamite?\n", "1866"},
      {"When did Thomas Edison invent phonograph?\n", "1877"},
      {"When did Mark Twain publish Huckleberry Finn?\n", "1884"},
      {"When did perfect Eastman the Kodak box camera?\n", "1888"},
      {"When did First World War begin in Europe?\n", "1914"},
      {"When did World War I end?\n", "1918"},
      {"When did Japan attack Pearl Harbor-U.S.?\n", "1941"},
      {"When did Compact discs first sold?\n", "1983"},
      {"When did Britain return Hong Kong colony to China?\n", "1997"},
  };

  private int getRandomNumber(int to) {
    Random random = new Random();
    return random.nextInt(to);
  }

  public String[] getEvent() {
    int index = getRandomNumber(questions.length);
    return questions[index];
  }
}
