package hw1.years;

import java.util.Arrays;
import java.util.Scanner;

public class Years {

  private int[] years = new int[0];
  private String name;
  private boolean game = true;


  private void print(String str) {
    System.out.print(str);
  }

  private void addYear(String year) {
    int tmpYear = Integer.parseInt(year);
    if (this.years.length == 0) {
      this.years = new int[1];
      this.years[0] = tmpYear;
      return;
    }
    int[] tmp = new int[this.years.length + 1];
    System.arraycopy(years, 0, tmp, 0, years.length);
    tmp[years.length] = tmpYear;
    this.years = tmp;
    Arrays.sort(this.years);
  }

  private boolean isNotNumeric(String str) {
    try {
      Integer.parseInt(str);
      return false;
    } catch (Exception e) {
      return true;
    }
  }

  private void congratulations() {
    System.out.printf("Congratulations, %s!\n", name);
  }

  private String[] arrIntToArrString() {
    String[] res = new String[years.length];
    for (int i = 0, ln = years.length; i < ln; i++) {
      res[i] = years[i] + "";
    }
    return res;
  }

  private void showEnteredYear() {
    String yearsStr = String.join(", ", arrIntToArrString());
    if (years.length > 1) {
      System.out.printf("Your years are %s\n", yearsStr);
    } else {
      System.out.printf("Your year is %s\n", yearsStr);
    }
  }

  public void game() {
    String answer;
    String[] events;
    final String yearIsSmall = "Your year is too small. Please, try again.\n";
    final String yearIsBig = "Your year is too big. Please, try again.\n";

    print("Let the game begin!\n");
    Scanner inName = new Scanner(System.in);
    print("Input your name: ");
    this.name = inName.nextLine();
    Questions question = new Questions();
    events = question.getEvent();
    Scanner inYear = new Scanner(System.in);
    print(events[0]);


    while (this.game) {
      answer = inYear.nextLine();
      if (isNotNumeric(answer)) {
        print("Input correct year!\n");
        continue;
      }
      addYear(answer);
      if (Integer.parseInt(answer) < Integer.parseInt(events[1])) {
        print(yearIsSmall);
      } else if (Integer.parseInt(answer) > Integer.parseInt(events[1])) {
        print(yearIsBig);
      } else {
        congratulations();
        showEnteredYear();
        this.game = false;
      }
    }
  }
}

