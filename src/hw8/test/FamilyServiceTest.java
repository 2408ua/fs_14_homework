package hw8.test;

import hw7.Family;
import hw7.human.Man;
import hw7.human.Woman;
import hw7.pet.Dog;
import hw7.pet.Pet;
import hw8.FamilyController;
import org.junit.jupiter.api.Test;
import org.testng.Assert;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {
  FamilyController controller;

  public FamilyServiceTest() {
    this.controller = new FamilyController();
    Dog pet = new Dog("Boy", 5, 49);
    Man father01 = new Man("Bob", "Marley", 1950, 90);
    Woman mother01 = new Woman("Mandy", "Marley", 1955, 100);

    Man father02 = new Man("Jon", "Black", 1993, 90);
    Woman mother02 = new Woman("Mandy", "Black", 1996, 100);

    Man father03 = new Man("Alex", "Cantor", 1983, 90);
    Woman mother03 = new Woman("April", "Cantor", 1980, 100);

    Man father04 = new Man("Charly", "Paterson", 1950, 90);
    Woman mother04 = new Woman("Agata", "Paterson", 1955, 100);

    Man father05 = new Man("Tri", "Bourne", 1975, 90);
    Woman mother05 = new Woman("Sara", "Bourne", 1985, 100);

    Man sun = new Man("Jeremy", "Ross", 1979, 95);
    Woman daughter = new Woman("Barbara", "Ross", 1990, 95);

    int index = controller.createNewFamily(mother01, father01);
    int index2 = controller.createNewFamily(mother02, father02);
    int index3 = controller.createNewFamily(mother03, father03);
    int index4 = controller.createNewFamily(mother04, father04);
    int index5 = controller.createNewFamily(mother05, father05);

    Family family01 = controller.getFamilyById(index);
    Family family02 = controller.getFamilyById(index2);
    Family family03 = controller.getFamilyById(index3);
    controller.bornChild(family01, "Cassy", "April");
    controller.adoptChild(family02, sun);
    controller.adoptChild(family03, daughter);
    controller.addPet(index3, pet);

  }

  @Test
  void getAllFamilies() {
    Assert.assertEquals(this.controller.getAllFamilies().size(), 5);
  }

  @Test
  void getFamiliesBiggerThan() {
    Assert.assertEquals(this.controller.getFamiliesBiggerThan(2).size(), 3);
  }

  @Test
  void getFamiliesLessThan() {
    Assert.assertEquals(this.controller.getFamiliesLessThan(3).size(), 2);
  }

  @Test
  void countFamiliesWithMemberNumber() {
    Assert.assertEquals(this.controller.countFamiliesWithMemberNumber(3), 3);
  }

  @Test
  void createNewFamily() {
    Man father03 = new Man("Alex", "Cantor", 1983, 90);
    Woman mother03 = new Woman("April", "Cantor", 1980, 100);
    int index6 = controller.createNewFamily(mother03, father03);
    Assert.assertEquals(index6, 5);
  }

  @Test
  void deleteFamilyByIndex() {
    this.controller.deleteFamilyByIndex(5);
    Assert.assertEquals(this.controller.getAllFamilies().size(), 5);
  }

  @Test
  void deleteFamilyByValue() {
    Family family = this.controller.getFamilyById(4);
    this.controller.deleteFamilyByValue(family);
    Assert.assertEquals(this.controller.getAllFamilies().size(), 4);
  }

  @Test
  void bornChild() {
    Family family = this.controller.getFamilyById(0);
    controller.bornChild(family, "Jon", "Cary");
    Assert.assertEquals(family.getChildren().size(), 2);
  }

  @Test
  void adoptChild() {
    Family family = this.controller.getFamilyById(0);
    Man sun = new Man("Jeremy", "Ross", 1979, 95);
    controller.adoptChild(family, sun);
    Assert.assertEquals(family.getChildren().size(), 2);
  }

  @Test
  void deleteAllChildrenOlderThen() {
    Family family = this.controller.getFamilyById(1);
    Assert.assertEquals(family.getChildren().size(), 1);
    this.controller.deleteAllChildrenOlderThen(21);
    Assert.assertEquals(family.getChildren().size(), 0);
  }

  @Test
  void count() {
    Assert.assertEquals(this.controller.count(), 5);
  }

  @Test
  void getFamilyById() {
    Man father05 = new Man("Tri", "Bourne", 1975, 90);
    Woman mother05 = new Woman("Sara", "Bourne", 1985, 100);
    Family familyTest = new Family(mother05, father05);
    Family family = this.controller.getFamilyById(4);
    Assert.assertTrue(family.equals(familyTest) && familyTest.equals(family));
    Assert.assertEquals(family.hashCode(), familyTest.hashCode());
  }

  @Test
  void getPets() {
    HashSet<Pet> pets01 = this.controller.getPets(1);
    HashSet<Pet> pets02 = this.controller.getPets(2);
    Assert.assertEquals(pets01.size(), 0);
    Assert.assertEquals(pets02.size(), 1);
  }

  @Test
  void addPet() {
    Dog pet = new Dog("Boy", 5, 49);
    HashSet<Pet> pets01 = this.controller.getPets(4);
    Assert.assertEquals(pets01.size(), 0);

    controller.addPet(4, pet);
    HashSet<Pet> pets02 = this.controller.getPets(4);
    Assert.assertEquals(pets02.size(), 1);
  }
}