package hw8;

import hw7.Family;

import java.util.*;

public interface FamilyDao {

  public ArrayList<Family> getAllFamilies();
  public Family getFamilyByIndex(int index);
  public Family getFamilyByValue(Family family);
  public boolean isFamilyNotExist(Family family);
  public boolean isFamilyNotExist(int index);
  public boolean deleteFamily(int index);
  public boolean deleteFamily(Family family);
  public int saveFamily(Family family);
}
