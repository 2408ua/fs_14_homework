package hw8;

import hw7.Family;

import java.util.*;

public class CollectionFamilyDao implements FamilyDao {
  ArrayList<Family> familyList;

  public CollectionFamilyDao() {
    this.familyList = new ArrayList<Family>();
  }

  @Override
  public ArrayList<Family> getAllFamilies() {
    return this.familyList;
  }

  @Override
  public Family getFamilyByIndex(int index) {
    return this.familyList.get(index);
  }

  @Override
  public Family getFamilyByValue(Family family) {
    if (this.familyList.size() == 0) return null;

    for (Family f : this.familyList) {
      if (f.equals(family) && (f.hashCode() == family.hashCode())) return f;
    }
    return null;
  }

  @Override
  public boolean isFamilyNotExist(Family family) {
    if (this.familyList.size() == 0) return true;

    for (Family f : this.familyList) {
      if (f.equals(family) && (f.hashCode() == family.hashCode())) return false;
    }
    return true;
  }

  @Override
  public boolean isFamilyNotExist(int index) {
    int size = this.familyList.size();
    if (size == 0 || index < 0 || index > size - 1) return true;
    return false;
  }

  @Override
  public boolean deleteFamily(int index) {
    try {
      int size = this.familyList.size();
      if (size == 0 || index < 0 || index > size - 1) return false;

      this.familyList.remove(index);
      return true;

    } catch (Exception e) {
      System.out.println(e);
      return false;
    }
  }

  @Override
  public boolean deleteFamily(Family family) {
    try {
      if (this.familyList.size() == 0) return false;

      int ln = this.familyList.size();
      for (int i = 0; i < ln; i++) {
        if (this.familyList.get(i).equals(family) && (this.familyList.get(i).hashCode() == family.hashCode())) {
          this.familyList.remove(i);
          return true;
        }
      }
      return false;

    } catch (Exception e) {
      System.out.println(e);
      return false;
    }
  }

  @Override
  public int saveFamily(Family family) {
    try {
      if (this.familyList.size() == 0) {
        this.familyList.add(family);
        return 0;
      }
      int ln = this.familyList.size();
      for (int i = 0; i < ln; i++) {
        if (this.familyList.get(i).equals(family) && (this.familyList.get(i).hashCode() == family.hashCode())) {
          this.familyList.set(i, family);
          return i;
        }
      }
      this.familyList.add(family);
      return this.familyList.size() - 1;

    } catch (Exception e) {
      System.out.println(e);
      return -1;
    }
  }
}
