package hw8;

import hw7.Family;
import hw7.human.Man;
import hw7.human.Woman;
import hw7.pet.Dog;

public class App {

  public static void main(String[] args) {
    FamilyController controller = new FamilyController();
    Dog pet = new Dog("Boy", 5, 49);
    Man father01 = new Man("Bob", "Marley", 1950, 90);
    Woman mother01 = new Woman("Mandy", "Marley", 1955, 100);

    Man father02 = new Man("Jon", "Black", 1993, 90);
    Woman mother02 = new Woman("Mandy", "Black", 1996, 100);

    Man father03 = new Man("Alex", "Cantor", 1983, 90);
    Woman mother03 = new Woman("April", "Cantor", 1980, 100);

    Man father04 = new Man("Charly", "Paterson", 1950, 90);
    Woman mother04 = new Woman("Agata", "Paterson", 1955, 100);

    Man father05 = new Man("Tri", "Bourne", 1975, 90);
    Woman mother05 = new Woman("Sara", "Bourne", 1985, 100);

    Man sun = new Man("Jeremy", "Ross", 1979, 95);
    Woman daughter = new Woman("Barbara", "Ross", 1990, 95);

    int index = controller.createNewFamily(mother01, father01);
    int index2 = controller.createNewFamily(mother02, father02);
    int index3 = controller.createNewFamily(mother03, father03);
    int index4 = controller.createNewFamily(mother04, father04);
    int index5 = controller.createNewFamily(mother05, father05);

    Family family01 = controller.getFamilyById(index);
    Family family02 = controller.getFamilyById(index2);
    Family family03 = controller.getFamilyById(index3);
    controller.bornChild(family01, "Cassy", "April");
    controller.adoptChild(family02, sun);
    controller.adoptChild(family03, daughter);
    controller.addPet(index3, pet);

    printLine("getAllFamilies");
    System.out.println(controller.getAllFamilies());

    printLine("displayAllFamilies");
    controller.displayAllFamilies();

    printLine("getFamiliesBiggerThan 2");
    controller.getFamiliesBiggerThan(2);

    printLine("getFamiliesLessThan 3");
    controller.getFamiliesLessThan(3);

    printLine("countFamiliesWithMemberNumber 3");
    System.out.println(controller.countFamiliesWithMemberNumber(3));

    controller.deleteFamilyByIndex(0);
    printLine("deleteFamilyByIndex");
    controller.displayAllFamilies();

    controller.bornChild(family02, "Cassy", "April");
    printLine("bornChild");
    controller.displayAllFamilies();

    controller.adoptChild(family03, sun);
    printLine("adoptChild");
    controller.displayAllFamilies();

    controller.deleteAllChildrenOlderThen(21);
    printLine("deleteAllChildrenOlderThen");
    controller.displayAllFamilies();

    printLine("count");
    System.out.println(controller.count());

    printLine("getFamilyById");
    System.out.println(controller.getFamilyById(2));

    printLine("getPets");
    System.out.println(controller.getPets(1));
  }

  public static void printLine(String name) {
    System.out.println("");
    System.out.print("############################################################################  ");
    System.out.print(name);
    System.out.println("  ############################################################################");
  }
}
