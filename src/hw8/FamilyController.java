package hw8;

import hw7.Family;
import hw7.human.Human;
import hw7.pet.Pet;

import java.util.*;

public class FamilyController {
  private final FamilyService service;

  public FamilyController() {
    this.service = new FamilyService();
  }

  public ArrayList<Family> getAllFamilies() {
    return this.service.getAllFamilies();
  }

  public void displayAllFamilies() {
    this.service.displayAllFamilies();
  }

  public ArrayList<Family> getFamiliesBiggerThan(int num) {
    return this.service.getFamiliesBiggerThan(num);
  }

  public ArrayList<Family> getFamiliesLessThan(int num) {
    return this.service.getFamiliesLessThan(num);
  }

  public int countFamiliesWithMemberNumber(int num) {
    return this.service.countFamiliesWithMemberNumber(num);
  }

  public int createNewFamily(Human mother, Human father) {
    return this.service.createNewFamily(mother, father);
  }

  public boolean deleteFamilyByIndex(int num) {
    return this.service.deleteFamilyByIndex(num);
  }

  public boolean deleteFamilyByValue(Family family) {
    return this.service.deleteFamilyByValue(family);
  }

  public boolean bornChild(Family family, String manName, String womanName) {
    return this.service.bornChild(family, manName, womanName);
  }

  public Family adoptChild(Family family, Human human) {
    return this.service.adoptChild(family, human);
  }

  public void deleteAllChildrenOlderThen(int age) {
    this.service.deleteAllChildrenOlderThen(age);
  }

  public int count() {
    return this.service.count();
  }

  public Family getFamilyById(int num) {
    return this.service.getFamilyById(num);
  }

  public HashSet<Pet> getPets(int familyIndex) {
    return this.service.getPets(familyIndex);
  }

  public boolean  addPet(int familyIndex, Pet pet) {
    return this.service.addPet(familyIndex, pet);
  }
}
